﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Net;
using OpenPop.Pop3;
using OpenPop.Mime;
using System.Text.RegularExpressions;
using System.Diagnostics;
using NLog;
using NLog.Targets;
using NLog.Config;
using System.Collections.Specialized;
using OpenPop.Mime.Decode;

namespace GetOrderForAllSources
{
    public class CfgLine
    {
        public string Memo; //Простолюбое имя
        public string Input; //Источник заявок pop3 ftp каталог
        public string Output; //Куда положить заявку
        public Filter FilterForLine; 
        public string ConvertFile; //Указание на конвертер, котрый нужно применить для заявки
        public string TempDirectory; //Временнная директория где будут лежать заявки когда закачаются из источника
        public int StatisticField; //Поле для сохранения количества заявок
        public string Prefix; //Поле для изменения имени файла заявки . Будет прибавляться префикс к файлу
    }
    public class Filter
    {
        public string MaskFile;
        public string Subject;
        public string Sender;
    }
    public class CfgConvert
    {
        public string name;
        public string cmd;
        public string outfile;
        public string path_for_original;
    }
    public class Processing
    {
        public static Logger log;
        public List<CfgLine> cfglines;
        public List<CfgConvert> cfgconvertlines;
        public ReadAppConfig appconfig;
        //Основная рабомая процедура 
        public bool DoProcessing()
        {
            try
            {
                log = LogManager.GetLogger("OrderLog");
                log.Trace("Стартуем {0}", DateTime.Now.ToString("dd.MM.yyyy HH:mm"));
                log.Trace("Читаем конфиг");
                appconfig = new ReadAppConfig();
                if (String.IsNullOrEmpty(appconfig.PathCfg)) log.Error("Не определен каталог для конфигов");
                log.Trace("PathCfg : {0}", appconfig.PathCfg);
                log.Trace("PathTemp : {0}", appconfig.PathTemp);
                log.Trace("PathConvert : {0}", appconfig.PathConvert);
                log.Trace("TimeOut : {0}", appconfig.TimeOut);
                //Если первый запуск создаем временную директорию
                if (!Directory.Exists(appconfig.PathTemp)) Directory.CreateDirectory(appconfig.PathTemp);
                //Проверку на удаление запускаем только при первом запуске 
                if (File.Exists(Path.Combine(appconfig.PathCfg, "statistics")) && File.GetLastWriteTime(Path.Combine(appconfig.PathCfg, "statistics")).Date < DateTime.Now.Date)
                {
                    log.Trace("Удаление архивных каталогов старше {0} дней" , appconfig.MaxDayLog);
                    DeleteOldFile(appconfig.MaxDayLog, appconfig.PathTemp);
                    DeleteOldFile(appconfig.MaxDayLog, appconfig.PathConvert);
                    if (File.Exists(Path.Combine(appconfig.PathTemp, "errors")))
                    {
                        File.Delete(Path.Combine(appconfig.PathTemp, "errors"));
                    }
                }
                //Проверить и сообщить о файлах содержащихся во временных каталогах
                CheckTemp(appconfig.PathTemp);
                cfglines = ReadOption();
                cfgconvertlines = ReadConvertOption();
                GetStatistics(appconfig.PathCfg, cfglines);
                //слишком много мусора
                //foreach (var cfgline in cfglines)
                //{
                //    log.Trace("Memo:{0} Input:{1} Output:{2} MaskFile:{3} Sender:{4} Convert:{5} TempDirectory:{6}", cfgline.Memo, cfgline.Input, cfgline.Output, cfgline.FilterForLine.MaskFile, cfgline.FilterForLine.Sender,cfgline.ConvertFile, cfgline.TempDirectory);
                //}
                //foreach (var cfgconvert in cfgconvertlines)
                //{
                //    log.Trace("Name:{0} Cmd:{1} Output file:{2} Path for original orders  :{3}", cfgconvert.name, cfgconvert.cmd, cfgconvert.outfile, cfgconvert.path_for_original);
                //}
                var inputsource = cfglines.GroupBy(c => c.Input).ToList();
                //1. Читаем конфигурационные файлы опрашиваем все источники данных и сохраняем данные на диск
                //Parallel.ForEach(inputsource, c => doinput(c.Key, cfglines));
                inputsource.ForEach(c => doinput(c.Key, cfglines));
                var outputsource = cfglines.GroupBy(c => c.Output).ToList();
                //2. Все файлы прочитанные в п. 1 отправляются до места назначения
                Parallel.ForEach(outputsource, c => dooutput(c.Key, cfglines));
                WriteStatistics(appconfig.PathCfg, cfglines);
                return true;
            }
            catch (Exception e)
            {
                System.IO.File.AppendAllText("c:\\temp\\logs.log", e.Message);
            }
            return false;
        }

        private void GetStatistics(string PathCfg, List<CfgLine> cfglines)
        {
            log.Trace("Получение статистики");
            try
            {
                if (File.Exists(Path.Combine(PathCfg, "statistics")))
                {
                    log.Trace("Найден файл {0}", Path.Combine(PathCfg, "statistics"));
                    List<string> statistics_file = File.ReadLines(Path.Combine(PathCfg, "statistics")).ToList();
                    if (DateTime.Now.ToString("dd.MM.yyyy") == statistics_file[0])
                    {
                        log.Trace("Статистика за {0}: ", DateTime.Now.ToString("dd.MM.yyyy"));
                        foreach (CfgLine cfgline in cfglines)
                        {
                            if (statistics_file.Find(c => c.Split(';')[0] == cfgline.Memo) != null)
                            {
                                log.Trace("Для {0} получено файлов {1}", cfgline.Memo, Convert.ToInt32(statistics_file.Find(c => c.Split(';')[0] == cfgline.Memo).Split(';')[1]));
                                cfgline.StatisticField = Convert.ToInt32(statistics_file.Find(c => c.Split(';')[0] == cfgline.Memo).Split(';')[1]);
                            }
                        }
                        log.Trace("Всего получено {0} файлов", cfglines.Sum(c => c.StatisticField).ToString());
                    }
                    else
                    {
                        File.Delete(Path.Combine(PathCfg, "statistics"));
                        log.Trace("Старый файл {0} статистики удаляется", Path.Combine(PathCfg, "statistics"));
                    }
                }
            }
            catch(Exception e)
            {
                log.Error("GetStatistics {0}", e.Message);
            }
        }

        private void WriteStatistics(string PathCfg, List<CfgLine> cfglines)
        {
            log.Trace("Запись статистики");
            try
            {
                StringBuilder statfile = new StringBuilder();
                statfile.Append(DateTime.Now.ToString("dd.MM.yyyy")).AppendLine();
                foreach (CfgLine cfgline in cfglines)
                {
                    statfile.Append(cfgline.Memo).Append(";").Append(cfgline.StatisticField).AppendLine();
                }
                File.WriteAllText(Path.Combine(PathCfg, "statistics"), statfile.ToString());
            }
            catch (Exception e)
            {
                log.Error("WriteStatistics {0}", e.Message);
            }
        }

        private void DeleteOldFile(string MaxDayLog,string TempPath)
        {
            try
            {
                //Удаление каталогов со старыми логами                     
                    string[] olddirs = Directory.GetDirectories(TempPath, "*", SearchOption.AllDirectories);
                    foreach (string olddir in olddirs)
                    {
                        int intMaxDayLog = Convert.ToInt16(MaxDayLog) + 1;
                        DateTime dt;
                        String dateDir = olddir.Split(Path.DirectorySeparatorChar).Last();
                        bool isValid = DateTime.TryParseExact(
                                         dateDir,
                                         "dd.MM.yyyy",
                                          System.Globalization.CultureInfo.InvariantCulture,
                                          System.Globalization.DateTimeStyles.None,
                                         out dt);
                        if (isValid)
                        {
                            if (dt <= DateTime.Now.Date.Subtract(new TimeSpan(intMaxDayLog, 0, 0, 0)))
                            {
                                if (Directory.Exists(olddir))
                                {
                                    Directory.Delete(olddir, true);
                                    log.Trace("Каталог {0} удален", olddir);
                                }
                            }
                        }
                    }
                          
            }
            catch (Exception e)
            {
                log.Error("DeleteFile {0}", e.Message);
            }
        }
        /// <summary>
        /// Проверка временных каталогов на предмет наличия файлов
        /// </summary>
        /// <param name="p"></param>
        private void CheckTemp(string PathTemp)
        {
            try
            {
                List<string>  files = new List<string>();
                Directory.GetDirectories(PathTemp).ToList().ForEach(c=>files.AddRange(Directory.GetFiles(c,"*")));
                //string[] files = Directory.GetFiles(PathTemp, "*", SearchOption.);
                List<string> listfiles = null;
                List<string> info = null; //список файлов отправляемых по почте
                if (File.Exists(Path.Combine(PathTemp,"listfile")))
                {
                    listfiles = File.ReadLines(Path.Combine(PathTemp, "listfile")).ToList();
                }                
                foreach (string file in files)
                {
                    if (listfiles != null)
                    {
                        if (listfiles.Find(c => c.Equals(file)) == null)
                        {
                            if(!file.Contains("listfile"))
                            {
                                info.Add(file);
                            }
                        }
                    }
                    else
                    { info.Add(file); }
                }
                if ( info !=null )
                {
                    log.Error("ВНИМАНИЕ!!! Не обработанные файлы {0}", string.Join(", ", info.ToArray()));
                }
                File.WriteAllLines(Path.Combine(PathTemp, "listfile"),files);
            }
            catch (Exception e)
            {
                log.Error("CheckTemp {0}", e.Message);
            }
        }
        /// <summary>
        /// Функция для фильтрации ошибок. Если ошибка повторяется то функция возращает false
        /// </summary>
        /// <param name="PathTemp"></param>
        /// <param name="instance"></param>
        /// <returns></returns>
        private Boolean CheckError(string PathTemp, errorObject instance)
        {
            // интервал установлен 1 час
            Encoding encoding = Encoding.GetEncoding(1251);
            int hour;
            if (!int.TryParse(appconfig.IntervalError, out hour)) { hour = 1; };
            TimeSpan interval =  new TimeSpan(hour, 0, 0);
            TimeSpan nowtime = DateTime.Now.TimeOfDay;
            Boolean rt = false;
            try
            {
                List<string> errorStrings = new List<string>();

                // ошибки пишутся в файл error
                if (File.Exists(Path.Combine(PathTemp, "errors")))
                {
                    errorStrings = File.ReadLines(Path.Combine(PathTemp, "errors"), encoding).ToList();
                }
                else
                {
                    errorStrings.Add(instance.errorProc + ';' + instance.inputOrder + ';' + instance.errorMessage + ';' + nowtime.ToString());
                    File.WriteAllLines(Path.Combine(PathTemp, "errors"), errorStrings, encoding);
                    return true;
                }
               
                foreach(var item in errorStrings)
                {
                   
                   string[] buffer = item.Split(';');
                    //если такая строка уже есть 
                   if (buffer.Length > 3 && instance.errorProc == buffer[0] && instance.inputOrder == buffer[1] && instance.errorMessage == buffer[2])
                     { 
                        //проверяем время
                        if (buffer[3] != null)
                        {
                            TimeSpan instanceTimeSpan;
                            var boolIf = TimeSpan.TryParse(buffer[3], out instanceTimeSpan);
                            //если ошибка живет меньше часа тогда ее не показывать
                            if (boolIf && (instanceTimeSpan > nowtime - interval)) { return false; }
                            //если ошибка живет больше часа тогда ее надо удалить
                            else
                            {
                                errorStrings.Remove(item);
                                File.WriteAllLines(Path.Combine(PathTemp, "errors"), errorStrings, encoding);
                                return false;
                            }
                        }
                    }
                }
                errorStrings.Add(instance.errorProc + ';' + instance.inputOrder + ';' + instance.errorMessage + ';' + nowtime.ToString());
                File.WriteAllLines(Path.Combine(PathTemp, "errors"), errorStrings, encoding);
                return true;
            }
            catch (Exception e)
            {
                log.Error("Checkerror {0}", e.Message);
            }
            return rt;
        }
        /// <summary>
        /// Обработка файлов 
        /// </summary>
        /// <param name="cfglines"></param>
        private void dooutput(string output, List<CfgLine> cfglines)
        {
            try
            {
                if (output.Contains("ftp://"))
                {
                    log.Trace("Вызов put_ftp:{0}", output);
                    put_ftp(cfglines, output); return;
                }
            }
            catch (Exception e)
            {
                log.Error("dooutput {0}", e.Message );
            }
            try
            {
                if (output.Contains("smtp"))
                {
                    log.Trace("Вызов put_smtp:{0}", output);
                    put_smtp(cfglines, output); return;
                }
             }
            catch (Exception e)
            {
                log.Error("dooutput {0}",e.Message);
            }
            try
            {
                log.Trace("Вызов put_in:{0}", output);
                put_in(cfglines, output);
            }
            catch (Exception e)
            {
                log.Error("dooutput {0}",e.Message);
            }
           
        }
        private void put_in(List<CfgLine> cfglines, string output)
        {
            try
            {
                foreach (var cfgline in cfglines.FindAll(c => c.Output == output))
                {
                    if (Directory.Exists(cfgline.TempDirectory))
                    {
                        //log.Trace("В каталоге {0} {1} файлов", cfgline.TempDirectory, Directory.GetFiles(cfgline.TempDirectory).Count());
                        foreach (string file in Directory.GetFiles(cfgline.TempDirectory))
                        {
                            try
                            {
                                if (!String.IsNullOrEmpty(cfgline.ConvertFile))
                                {
                                    if (!IsFileLocked(new FileInfo(file)))
                                    {
                                        log.Trace("Для файла {0} нужна конвертация {1}", file, cfgline.ConvertFile);
                                        //Добавил копирование в архив во временной директории папка - текущая дата
                                        if (!Directory.Exists(Path.Combine(cfgline.TempDirectory, DateTime.Now.ToString("dd.MM.yyyy")))) 
                                            Directory.CreateDirectory(Path.Combine(cfgline.TempDirectory, DateTime.Now.ToString("dd.MM.yyyy")));
                                        File.Copy(file, Path.Combine(Path.Combine(cfgline.TempDirectory, DateTime.Now.ToString("dd.MM.yyyy")), Path.GetFileName(file)), true);
                                        string[] convertfiles = doconvert(cfgline, file);
                                        foreach (string convertfile in convertfiles)
                                        {
                                            File.Copy(convertfile, Path.Combine(output, Path.GetFileName(convertfile)), true);
                                            File.Delete(convertfile);
                                            //cfgline.StatisticField++;
                                            log.Info("Файл {0} отправлен в {1}", convertfile, output);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!IsFileLocked(new FileInfo(file)))
                                    {
                                        if (!Directory.Exists(Path.Combine(cfgline.TempDirectory, DateTime.Now.ToString("dd.MM.yyyy"))))
                                            Directory.CreateDirectory(Path.Combine(cfgline.TempDirectory, DateTime.Now.ToString("dd.MM.yyyy")));
                                        File.Copy(file, Path.Combine(Path.Combine(cfgline.TempDirectory, DateTime.Now.ToString("dd.MM.yyyy")), Path.GetFileName(file)), true);
                                        File.Copy(file, Path.Combine(output, Path.GetFileName(file)), true);
                                        File.Delete(file);
                                        //cfgline.StatisticField++;
                                        log.Info("Файл {0} отправлен в {1}", file, output);
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                log.Error("put in(). Файл: {0}. Конвертер: {1}. {2} Memo: {3}", file, cfgline.ConvertFile, e.Message, cfgline.Memo);
                            }

                        }
                    }
                }
                
            }
            catch (Exception e)
            {
                log.Error("put in {0} {1}", output, e.Message);
            }
        }
        /// <summary>
        /// Вызов конвертора
        /// </summary>
        /// <param name="ConvertFile"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        private string[] doconvert(CfgLine CfgLine, string file)
        {
            StringCollection values = new StringCollection();
            int exitCode;
            //string cmd = "";
            //string outfile = "";
            //string originalfilepath = "";
            CfgConvert cfgconvert;
            try
            {
                #region Старый код
                //string[] buffers = File.ReadAllLines(Path.Combine(appconfig.PathConvert, CfgLine.ConvertFile));
                //foreach (string buffer in buffers)
                //{
                //    if (!buffer.Contains("#"))
                //    {
                //        string[] bufs = buffer.Split(';');
                //        if (bufs[0].Trim() == "cmd") cmd = bufs[1].TrimStart();
                //        if (bufs[0].Trim() == "out") outfile = bufs[1].TrimStart();
                //        if (bufs[0].Trim() == "original") originalfilepath = bufs[1].TrimStart();
                //    }
                //}
                #endregion
                cfgconvert = cfgconvertlines.Find(c => c.name.ToUpper() == CfgLine.ConvertFile.ToUpper());
                // Prepare the process to run
                ProcessStartInfo start = new ProcessStartInfo();
                // Enter in the command line arguments, everything you would enter after the executable name itself
                start.Arguments = cfgconvert.cmd.Substring(cfgconvert.cmd.IndexOf(' ')).Replace("%1", file);
                // Enter the executable to run, including the complete path
                start.FileName = cfgconvert.cmd.Split(' ')[0];
                start.WorkingDirectory = Path.GetDirectoryName(start.FileName);
                // Do you want to show a console window?
                start.WindowStyle = ProcessWindowStyle.Hidden;
                start.RedirectStandardOutput = true;
                start.RedirectStandardError = true;
                start.UseShellExecute = false;
                start.CreateNoWindow = true;
                log.Trace("Запуск конвертера {0} {1}", start.FileName, start.Arguments);
                // Run the external process & wait for it to finish
                using (Process proc = Process.Start(start))
                {
                    proc.OutputDataReceived += (s, e) =>
                    {
                        lock (values)
                        {
                            values.Add(e.Data);
                        }
                    };
                    proc.ErrorDataReceived += (s, e) =>
                    {
                        lock (values)
                        {
                            values.Add("! > " + e.Data);
                        }
                    };

                    proc.BeginErrorReadLine();
                    proc.BeginOutputReadLine();

                    proc.WaitForExit();
                    foreach (string sline in values)
                        log.Trace("Вывод программы {0}: {1}", start.FileName, sline); ;              
                    exitCode = proc.ExitCode;
                }
                if (exitCode == 0)
                {
                    // Если указан путь файл ищется по этому пути, иначе путь ищется в рабочей директории конвертера
                    string namefileforreplacing;
                    if (String.IsNullOrEmpty(Path.GetDirectoryName(cfgconvert.outfile)))
                    { namefileforreplacing = file; } // Усли указано выходное имя файла без директории
                    else
                    { namefileforreplacing = Path.GetFileName(file); } //если указано выходное имя с директорией
                    // Если в конфиге указан параметр %1 значит работаем с одним файлом
                    if (cfgconvert.outfile.Contains("%1"))
                    {

                        if (File.Exists(cfgconvert.outfile.Replace("%1", namefileforreplacing)))
                            {
                            //Если не нужно сохранять оригинальный файл то удаляем
                                if (String.IsNullOrEmpty(cfgconvert.path_for_original))
                                {
                                    File.Delete(file);
                                }
                                else
                                {
                                    if (!Directory.Exists(cfgconvert.path_for_original.Replace("%date%", DateTime.Now.ToString("dd.MM.yyyy"))))
                                        Directory.CreateDirectory(cfgconvert.path_for_original.Replace("%date%", DateTime.Now.ToString("dd.MM.yyyy")));
                                    File.Copy(file, Path.Combine(cfgconvert.path_for_original.Replace("%date%", DateTime.Now.ToString("dd.MM.yyyy")), Path.GetFileName(file)), true);
                                    File.Delete(file);
                                }
                            }
                        log.Trace("Файл после конвертации {0}", cfgconvert.outfile.Replace("%1", namefileforreplacing));
                        return new string[] { cfgconvert.outfile.Replace("%1", namefileforreplacing) };                   
                    }
                        // Работаем с маской файлов . Сделано для заявок в архиве
                    else
                    {
                        if (String.IsNullOrEmpty(cfgconvert.path_for_original))
                        {
                            if (File.Exists(file))
                                File.Delete(file);
                        }
                        else
                        {
                            if (!Directory.Exists(cfgconvert.path_for_original.Replace("%date%", DateTime.Now.ToString("dd.MM.yyyy"))))
                                Directory.CreateDirectory(cfgconvert.path_for_original.Replace("%date%", DateTime.Now.ToString("dd.MM.yyyy")));
                            File.Copy(file, Path.Combine(cfgconvert.path_for_original.Replace("%date%", DateTime.Now.ToString("dd.MM.yyyy")), Path.GetFileName(file)), true);
                            File.Delete(file);
                        }             //то же самое если в шаблоне выходного файла только файл , то ищем в каталоге входного файла, если указан каталог то ищем файлы именно там
                        return Directory.GetFiles(String.IsNullOrEmpty(Path.GetDirectoryName(cfgconvert.outfile)) ? Path.GetDirectoryName(file) : Path.GetDirectoryName(cfgconvert.outfile), Path.GetFileName(cfgconvert.outfile));

                    }
                }
              
            }
            catch (Exception e)
            {
                log.Error("docovert {0} {1}", file, e.Message);
            }
            return null;
        }
        private void put_smtp(List<CfgLine> cfglines, string output)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Отпраляем на ФТП 
        /// </summary>
        /// <param name="cfglines"></param>
        /// <param name="output"></param>
        private void put_ftp(List<CfgLine> cfglines, string output)
        {
            try
            {
                foreach (var cfgline in cfglines.FindAll(c => c.Output == output))
                {
                    if (!String.IsNullOrEmpty(cfgline.ConvertFile))
                    {
                        foreach (string file in Directory.GetFiles(cfgline.TempDirectory))
                        {
                            if (!IsFileLocked(new FileInfo(file)))
                            {
                                if (!Directory.Exists(Path.Combine(cfgline.TempDirectory, DateTime.Now.ToString("dd.MM.yyyy"))))
                                    Directory.CreateDirectory(Path.Combine(cfgline.TempDirectory, DateTime.Now.ToString("dd.MM.yyyy")));
                                File.Copy(file, Path.Combine(Path.Combine(cfgline.TempDirectory, DateTime.Now.ToString("dd.MM.yyyy")), Path.GetFileName(file)), true);
                                string[] convertfiles = doconvert(cfgline, file);
                                foreach (string convertfile in convertfiles)
                                {
                                    UploadFile(new string[] { convertfile }, output);
                                    //cfgline.StatisticField++;
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (string file in Directory.GetFiles(cfgline.TempDirectory))
                        {
                            if (!Directory.Exists(Path.Combine(cfgline.TempDirectory, DateTime.Now.ToString("dd.MM.yyyy"))))
                                Directory.CreateDirectory(Path.Combine(cfgline.TempDirectory, DateTime.Now.ToString("dd.MM.yyyy")));
                            File.Copy(file, Path.Combine(Path.Combine(cfgline.TempDirectory, DateTime.Now.ToString("dd.MM.yyyy")), Path.GetFileName(file)), true);
                            if (!IsFileLocked(new FileInfo(file)))
                            {
                                UploadFile(new string[] { file }, output);
                                //cfgline.StatisticField++;
                            }
                        }
                        

                    }
                }
            }
            catch (Exception e)
            {
                log.Error("{0} {1}", output, e.Message);
            }
            
        }
        /// <summary>
        /// Обработка одной строки из конфиг файла и получение файла из источника и сохранение на диск
        /// </summary>
        /// <param name="line"></param>
        private void doinput(string input, List<CfgLine> cfglines)
        {
            try
            {
                if (input.Contains("pop3:") == true)
                {
                    log.Trace("Вызов pop3:{0}", input);
                    get_pop3(input, cfglines); return;
                }
            }
            catch (Exception e)
            {
                log.Error("doinput {0}", e.Message);
            }
            try
            {
                if (input.Contains("ftp:") == true)
                {
                    log.Trace("Вызов ftp:{0}", input);
                    get_ftp(input, cfglines); return;
                }
            }
            catch (Exception e)
            {
                log.Error("doinput {0}", e.Message);
            }
            try
            {
                //Если вход просто каталог
                log.Trace("Вызов in:{0}", input);
                get_in(input, cfglines);
            }
            catch (Exception e)
            {
                log.Error("doinput {0}", e.Message);
            }

        }

        private void get_in(string input, List<CfgLine> cfglines)
        {
            try
            {
                foreach (CfgLine cfgline in cfglines.FindAll(c => c.Input == input))
                {
                    string pattern = cfgline.FilterForLine.MaskFile;
                    if (!Directory.Exists(cfgline.TempDirectory)) Directory.CreateDirectory(cfgline.TempDirectory);
                    log.Trace("Найдено {0} файлов по маске {1} в {2}", Directory.GetFiles(input, cfgline.FilterForLine.MaskFile).Count() ,cfgline.FilterForLine.MaskFile,  Path.Combine(cfgline.TempDirectory));
                    foreach (string file in Directory.GetFiles(input, cfgline.FilterForLine.MaskFile))
                    {
                        try
                        {
                            if (!IsFileLocked(new FileInfo(file)))
                            {
                               
                                string file_with_prefix = CreateNameFileWithPrefix(Path.GetFileName(file),cfgline);
                                File.Copy(file, Path.Combine(cfgline.TempDirectory, file_with_prefix), true);
                                if (File.Exists(Path.Combine(cfgline.TempDirectory, file_with_prefix)))
                                {
                                    File.Delete(file);
                                    cfgline.StatisticField++;
                                }

                                log.Info("Файл {0}  перемещен в {1}", file, cfgline.FilterForLine.MaskFile, Path.Combine(cfgline.TempDirectory));
                            }
                        }
                        catch(Exception e)
                        {
                            log.Error("doinput: Ошибка в файле {0}{1} Memo:{2} ", file, e.Message , cfgline.Memo);
                        }
                    }
                }
            }
            catch (Exception e)
            { log.Error("getin {0} {1}", input, e.Message); }
        }
        /// <summary>
        /// Создание префикса файла из исходного 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="cfgline"></param>
        /// <returns></returns>
        private string CreateNameFileWithPrefix(string file,CfgLine cfgline)
        {
            try
            {
            if (!String.IsNullOrEmpty(cfgline.Prefix)) 
            {
                string Prefix1 = cfgline.Prefix.Replace("%DATE%",DateTime.Now.ToString("dd_MM_yyyy"))
                                            .Replace("%date%",DateTime.Now.ToString("dd_MM_yyyy"))
                                            .Replace("%Date%",DateTime.Now.ToString("dd_MM_yyyy"));
                string Prefix2 = Prefix1.Replace("%TIME$",DateTime.Now.ToString("HH_mm_ss"))
                                            .Replace("%time%",DateTime.Now.ToString("HH_mm_ss"))
                                            .Replace("%Time%",DateTime.Now.ToString("HH_mm_ss"));
                string Prefix3 = Prefix2.Replace("%Number%", cfgline.StatisticField.ToString()).Replace("%number%",cfgline.StatisticField.ToString()).Replace("%Number%",cfgline.StatisticField.ToString());
                string rt = PathSanitizer.SanitizeFilename(Prefix3, '_'); 
                log.Trace("CreateNameFileWithPrefix: Префикс{0} Исходная строка в конфиге {1} ", rt, cfgline.Prefix);
                return rt+file;
            }
            }
            catch (Exception e)
            {
                log.Error("CreateNameFileWithPrefix: Ошибка создания префикса для {0} {1}", cfgline.Memo, e.Message , cfgline.Memo);
            }
            return  PathSanitizer.SanitizeFilename(file, '_');
        }
        /// <summary>
        /// Закачка файлов с фтп и 
        /// </summary>
        /// <param name="line"></param>
        private void get_ftp(string input, List<CfgLine> cfglines)
        {
            try
            {
                List<string> files = GetFileList(GetFTPUri(input), isPassive(input));

                foreach (var cfgline in cfglines.FindAll(c => c.Input == input))
                {
                    try
                    {
                        string pattern = '^' + Regex.Escape(cfgline.FilterForLine.MaskFile)
                                                    .Replace(@"\?", ".")
                                                    .Replace(@"\*", ".*") + '$'; 
                        var matches = files.Where(path => Regex.Match(path, pattern).Success);
                        log.Trace("Найдено {0} файлов по маске {1} в {2}", matches.Count(), cfgline.FilterForLine.MaskFile, cfgline.Memo);
                        foreach (string file in matches)
                        {
                            if (!CheckSizeFileonFtp(GetFTPUri(input), file, isPassive(input))) continue;
                            if (DownloadDeleteFile(GetFTPUri(input), file, cfgline, isPassive(input))) cfgline.StatisticField++; ; //Если файл  загружен счетчик увеличиваем
                            log.Info("Файл {0}  перемещен в {1}", file, cfgline.TempDirectory);
                        }
                    }
                    catch (Exception e)
                    {
                        log.Error("get_ftp {0} {1} Memo: {2}", input, e.Message , cfgline.Memo);
                    }
                }
            }
            catch (Exception e)
            {      
                    log.Error("get_ftp {0} {1}", input, e.Message);       
            }
        }

        private bool CheckSizeFileonFtp(string FullPathFtp, string file, Boolean isPassive = true)
        {
            try
            {
                    long size;     
                    var regFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(FullPathFtp + file));
                    regFTP.Method = WebRequestMethods.Ftp.GetFileSize;
                    regFTP.UseBinary = true;
                    regFTP.Proxy = null;
                    regFTP.UsePassive = isPassive;
                    regFTP.Timeout = 50000;
                    using (FtpWebResponse response = (FtpWebResponse)regFTP.GetResponse())
                    { size = response.ContentLength; }
                log.Trace("CheckSizeFileonFtp : Размер файла  {0} на сервере {1}  определен. {2}", file, FullPathFtp, size);
                return true;
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (response.StatusCode != FtpStatusCode.ActionNotTakenFileUnavailable)
                {
                    log.Trace("CheckSizeFileonFtp : Размер файла  {0} на сервере {1} не определен. Загружаться не будет", file, FullPathFtp);
                }
                else
                {
                    log.Trace("CheckSizeFileonFtp : Размер файла  {0} на сервере {1} не  определен. Возможно сервер не поддерживает этот запрос", file, FullPathFtp);
                    return true;
                }
            }
            return false; 
        }

        private bool isFileExistsOnFtp(string FullPathFtp, string file, Boolean isPassive = true)
        {
            try
            {
                DateTime fileDate;
                var regFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(FullPathFtp + file));
                regFTP.Method = WebRequestMethods.Ftp.GetDateTimestamp;
                regFTP.UseBinary = true;
                regFTP.Proxy = null;
                regFTP.UsePassive = isPassive;
                regFTP.Timeout = 50000;
                using (FtpWebResponse response = (FtpWebResponse)regFTP.GetResponse())
                { fileDate = response.LastModified; }
                log.Trace("CheckFileExistsOnFtp : Дата файла  {0} на сервере {1}  определена. {2}", file, FullPathFtp, fileDate);
                return true;
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                log.Trace("CheckFileExistsOnFtp : Дата файла  {0} на сервере {1}  не определена.", file, FullPathFtp);
            }
            return false;
        }

        private void get_pop3(string input, List<CfgLine> cfglines)
        {
            try
            {
                EncodingFinder.FallbackDecoder = CustomFallbackDecoder;
               // EncodingFinder.AddMapping("base64", Encoding.UTF8);
                string[] buffer = input.Split(':');
                string pop3server = buffer[3];
                string password = buffer[2];
                string username = buffer[1];
                int port=0;
                if (buffer.Length > 4) int.TryParse(buffer[4], out port);
                if (port == 0) port = 110;
                using (OpenPop.Pop3.Pop3Client client = new OpenPop.Pop3.Pop3Client())
                {
                    if (port == 110) client.Connect(pop3server, port, false);
                    else client.Connect(pop3server, port, true, 200, 200, certificateValidator);
                    client.Authenticate(username, password, AuthenticationMethod.UsernameAndPassword);
                    if (client.Connected)
                    {
                        int messageCount = client.GetMessageCount();
                        log.Trace("В mailbox {0} на сервере {1} {2} сообщений",cfglines.First(c=>c.Input==input).Memo , pop3server, messageCount.ToString());
                        List<Message> allMessages = new List<Message>(messageCount);
                        for (int i = messageCount; i > 0; i--)
                        {
                            try
                            {
                            allMessages.Add(client.GetMessage(i));
                            log.Trace("get_pop3:Добавлено письмо на обработку mailbox {0} на сервере {1} отправитель {2}  ", cfglines.First(c => c.Input == input).Memo, pop3server, client.GetMessage(i).Headers.From.Address);
                            }
                            catch (Exception e)
                            {
                                log.Error(String.Format("get_pop3: {0}, отправитель  {1} строка {2}" ,e.Message, client.GetMessage(i).Headers.From.Address , input));
                            }
                        }
                        //Все письма вложения писем без правил кладутся в UKNOWNRULE каталог
                        string output_all = Path.Combine(appconfig.PathTemp, "UKNOWNRULE");
                        if (cfglines.First(c => c.Input == input && String.IsNullOrEmpty(c.FilterForLine.Sender)) != null)
                        {
                            output_all = cfglines.First(c => c.Input == input && String.IsNullOrEmpty(c.FilterForLine.Sender)).TempDirectory;
                        }
                        int j = allMessages.Count+1; 
                        List<int> nomera_mess_for_del = new List<int>();
                        log.Trace("get_pop3: Начинаем цикл обработки вложений mailbox {0} на сервере {1} ", cfglines.First(c => c.Input == input).Memo, pop3server);
                        foreach (Message msg in allMessages)
                        {
                            #region Цикл обработки писем 
                            string id = msg.Headers.MessageId;
                            j--; 
                            try
                            {
                                string sender = msg.Headers.From.Address;
                                string subject = msg.Headers.Subject;
                                var att = msg.FindAllAttachments();
                                bool save_success = true ;
                                log.Info("Получено письмо mailbox {0} на сервере {1}  от отправителя {2}", cfglines.First(c => c.Input == input).Memo, pop3server, sender);
                                if (cfglines.Find(c => c.Input == input && CheckSender(c.FilterForLine.Sender, sender)) != null)
                                {
                                    #region Если указан отправитель в конфигурационном файле
                                    CfgLine cfgline = cfglines.Find(c => c.Input == input && CheckSender(c.FilterForLine.Sender, sender));
                                    string output_filter_sender = cfgline.TempDirectory;
                                    if (!Directory.Exists(output_filter_sender)) Directory.CreateDirectory(output_filter_sender);
                                   
                                    if (cfgline.FilterForLine.MaskFile.ToUpper().Contains("BODY2FILE"))
                                    {
                                        #region  Проверяем нужно ли заявку из тела письма вытащить
                                        string file_with_prefix = CreateNameFileWithPrefix(sender + ".msg", cfgline);
                                         OpenPop.Mime.MessagePart body;
                                         body =  msg.FindFirstPlainTextVersion();
                                         if (body == null)
                                             body = msg.FindFirstHtmlVersion();
                                         if (body != null)
                                         {
                                             Encoding wind1251 = Encoding.GetEncoding(1251);
                                             byte[] BodyBytes = msg.MessagePart.BodyEncoding.GetBytes(body.GetBodyAsText());
                                             byte[] wind1251Bytes = Encoding.Convert(msg.MessagePart.BodyEncoding, wind1251, BodyBytes);
                                             string BodyStringWin1251 = wind1251.GetString(wind1251Bytes);
                                             File.AppendAllText(System.IO.Path.Combine(output_filter_sender, file_with_prefix), BodyStringWin1251, wind1251);
                                             if (File.Exists(System.IO.Path.Combine(output_filter_sender, file_with_prefix)))
                                             {
                                                 log.Info("{0} Subj{1} Текст письма сохранен в файл {2} от отправителя {3} перемещен {4}", cfgline.Memo, subject, file_with_prefix, sender, output_filter_sender);
                                                 cfgline.StatisticField++;
                                             }
                                             else
                                             {
                                                 log.Error("{0} Subj{1} Текст письма не удалось сохранить в файл {2} от отправителя {3} перемещен {4}", cfgline.Memo, subject, file_with_prefix, sender, output_filter_sender);
                                                 save_success = false;
                                             }
                                         }
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Заявка - прикрепленный файл
                                        foreach (var ado in att)
                                        {
                                            if (!Checkmaskfile(ado.FileName, cfgline.FilterForLine.MaskFile)) continue;
                                            string file_with_prefix = CreateNameFileWithPrefix(ado.FileName, cfgline);
                                            ado.Save(new System.IO.FileInfo(System.IO.Path.Combine(output_filter_sender, file_with_prefix)));
                                            if (File.Exists(System.IO.Path.Combine(output_filter_sender, file_with_prefix)))
                                            {
                                                log.Info("{0} Subj{1} файл {2} от отправителя {3} перемещен {4}", cfgline.Memo, subject, ado.FileName.Replace("\\", "_"), sender, output_filter_sender);
                                                cfgline.StatisticField++;
                                            }
                                            else
                                            {
                                                log.Error("{0} Subj{1} файл {2} от отправителя {3} не удалось переместить {4}", cfgline.Memo, subject, ado.FileName.Replace("\\", "_"), sender, output_filter_sender);
                                                save_success = false;
                                            }
                                        }
                                        #endregion
                                    }
                                    #endregion
                                }                               
                                else
                                {
                                    #region Если не указан отправитель в конфигурационном файле
                                    //Опять проверка на заявка из тела письма
                                    if (!Directory.Exists(output_all)) Directory.CreateDirectory(output_all);
                                    CfgLine cfgline = null;
                                    string file_with_prefix;
                                    //Проверяем есть ли строка в кониге , настроенная на забор всей почты 
                                    if (cfglines.Find(c => c.Input == input && String.IsNullOrEmpty(c.FilterForLine.Sender)) != null)
                                    {
                                        cfgline = cfglines.Find(c => c.Input == input && String.IsNullOrEmpty(c.FilterForLine.Sender));                                 
                                    }
                                   
                                    if (cfgline.FilterForLine.MaskFile.ToUpper().Contains("BODY2FILE"))
                                    {
                                        #region  Проверяем нужно ли заявку из тела письма вытащить
                                        file_with_prefix = CreateNameFileWithPrefix(sender + ".msg", cfgline);
                                        OpenPop.Mime.MessagePart body;
                                        body = msg.FindFirstPlainTextVersion();
                                        if (body == null)
                                            body = msg.FindFirstHtmlVersion();
                                        if (body != null)
                                        {
                                            Encoding wind1251 = Encoding.GetEncoding(1251);
                                            byte[] BodyBytes = msg.MessagePart.BodyEncoding.GetBytes(body.GetBodyAsText());
                                            byte[]  wind1251Bytes = Encoding.Convert( msg.MessagePart.BodyEncoding,wind1251,BodyBytes);
                                            string BodyStringWin1251 = wind1251.GetString(wind1251Bytes);                
                                            File.AppendAllText(System.IO.Path.Combine(output_all, file_with_prefix), BodyStringWin1251, wind1251);
                                            if (File.Exists(System.IO.Path.Combine(output_all, file_with_prefix)))
                                            {
                                                log.Info("{0} Subj{1} Текст письма сохранен в файл {2} от отправителя {3} перемещен {4}", cfgline.Memo, subject, file_with_prefix, sender, output_all);
                                                cfgline.StatisticField++;
                                            }
                                            else
                                            {
                                                log.Error("{0} Subj{1} Текст письма не удалось сохранить в файл {2} от отправителя {3} перемещен {4}", cfgline.Memo, subject, file_with_prefix, sender,output_all);
                                                save_success = false;
                                            }
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Заявка - прикрепленный файл
                                        foreach (var ado in att)
                                        {
                                            if (cfgline == null) file_with_prefix = ado.FileName;
                                            else file_with_prefix = CreateNameFileWithPrefix(ado.FileName, cfgline);

                                            if (!Checkmaskfile(ado.FileName, cfgline.FilterForLine.MaskFile)) continue;
                                            ado.Save(new System.IO.FileInfo(System.IO.Path.Combine(output_all, file_with_prefix)));
                                            //Проверяем что файл выгружен иначе не будем удалять письмо
                                            if (File.Exists(System.IO.Path.Combine(output_all, file_with_prefix)))
                                            {
                                                if (cfgline != null) cfgline.StatisticField++;
                                                log.Info("{0} Subj: {1} файл {2} от отправителя {3} перемещен {4}", cfglines.First(c => c.Input == input).Memo, subject, ado, sender, output_all);
                                                if (output_all == "UKNOWNRULE")
                                                    log.Error("{0} Subj: {1} файл {2} от отправителя {3} перемещен в UKNOWNRULE. Правило не найдено", cfglines.First(c => c.Input == input).Memo, subject, ado, sender);
                                            }
                                            else
                                            {
                                                log.Error("{0} Subj: {1} файл {2} от отправителя {3} не удалось переместить {4}", cfglines.First(c => c.Input == input).Memo, subject, ado.FileName, sender, output_all);
                                                save_success = false;
                                            }
                                        }
                                        #endregion
                                    }

                                    #endregion
                                }
                                if (save_success)
                                {
                                    if (client.GetMessageHeaders(j).MessageId == msg.Headers.MessageId)
                                    {
                                        client.DeleteMessage(j); //Удаляем письмо
                                        log.Trace(String.Format("get_pop3: Письмо удалено из ящика. отправитель {0}, тема письма {1}",
                                                                                                                      msg.Headers.From.Address,
                                                                                                                       msg.Headers.Subject));
                                    }
                                    else
                                    {
                                        log.Error(String.Format("get_pop3: Письмо не удалено из ящика. отправитель {0},тема письма {1} ",
                                                                                                                      msg.Headers.From.Address,
                                                                                                                      msg.Headers.Subject));
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                 log.Error(String.Format("get_pop3: {0}, отправитель {1}",e.Message , msg.Headers.From.Address));
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception e)
            { log.Error("get_pop3 {0} {1} сервер {2} ",input, e.Message,input); }
        }

        private bool CheckSender(string filter, string sender)
        {
            // Check domain filtr
            if (filter.StartsWith("@"))
            {
                if (sender.Contains(filter)) return true;
            }
            if (filter.Contains(sender)) return true;
            return false;
        }

        /// <summary>
        /// Проверка маски файла Если файл подходит под маску, то true
        /// </summary>
        /// <param name="filename">имя проверяемого файла</param>
        /// <param name="maskfile">маска файла *.com,!html</param>
        /// <returns></returns>
        private bool Checkmaskfile(string filename, string maskfile)
        {
            bool rsp = false;
            if (String.IsNullOrEmpty(maskfile)) return true;
            string[] buffers = maskfile.Split(',');
            foreach (var buffer in buffers.Where(c => !c.Trim().StartsWith("!")))
            {
                string pattern = Regex.Escape(buffer).Replace(@"\?", ".").Replace(@"\*", ".*"); ;
                if (Regex.Match(filename, pattern).Success) return true;
                rsp = false;
            }
            //Проверка на антимаску !*.html
            foreach (var buffer in buffers.Where(c => c.Trim().StartsWith("!")))
            {
                string antimask = buffer.Trim().Replace("!", string.Empty);
                string pattern = Regex.Escape(antimask).Replace(@"\?", ".").Replace(@"\*", ".*"); ;
                if (Regex.Match(filename, pattern).Success) return false;
                rsp = true;
            }
           
         
            return rsp;
        }

        private bool certificateValidator(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
        /// <summary>
        /// Чтение конфигурационных файлов
        /// </summary>
        /// <param name="PathCfg"></param>
        /// <returns></returns>
        private List<CfgLine> ReadOption()
        {
            log.Trace("Читаем конфиги ");
            List<CfgLine> cfglines = new List<CfgLine>();
            try
            {
                string[] files = Directory.GetFiles(appconfig.PathCfg, "*.cfg");
                log.Trace("Количество найденных файлов {0}",files.Length.ToString() );
                if (files.Length == 0)
                    log.Trace("Не найден ни один конфигурационный файл в каталоге {0}", appconfig.PathCfg);
                foreach (string file in files)
                {
                    try
                    {
                        log.Trace("Найден конфигурационный файл {0}", file);
                        string[] lines = File.ReadAllLines(file);
                        for (int i = 0; i < lines.Length; i++)
                        {
                            try
                            {
                                if (!String.IsNullOrEmpty(lines[i]) && !lines[i].TrimStart().StartsWith("#"))
                                {

                                    string[] buffer = lines[i].Split(';');
                                    //Если нужно конвертировать файл, или нужно отправить по фтп, то он сохраняется в буфере ftp.log@nameftpserver_path 
                                    //Если назначение каталог , то временная папка этот каталог
                                    string TempDirectory = CreateNameTempDirectory(buffer[0].Trim(), buffer[1].Trim(), buffer.Length >= 6 ? buffer[5].Trim() : "");
                                    string Prefix = buffer.Length >= 7 ? buffer[6].Trim() : "";
                                    string Subject = buffer.Length >= 8 ? buffer[7].Trim() : "";
                                    cfglines.Add(new CfgLine {   StatisticField = 0, 
                                                                 Memo = buffer[0].Trim(), 
                                                                 Input = buffer[1].Trim(), 
                                                                 Output = buffer[2].Trim(), 
                                                                 ConvertFile = buffer.Length >= 5 ? buffer[4].Trim() : "", 
                                                                 FilterForLine = new Filter { Sender = buffer.Length >= 6 ? buffer[5].Trim() : "", 
                                                                 MaskFile = buffer[3].Trim(), Subject = Subject }, 
                                                                 TempDirectory = TempDirectory,
                                                                 Prefix =Prefix});
                                }
                            }
                            catch (Exception e)
                            { log.Error("ReadConfig {0} ошибка в строке {1} в файле {2}", e.Message,i,file); }
                        }
                    }
                    catch (Exception e)
                    { log.Error("ReadConfig {0} в файле {1}", e.Message,file); }
                }
            }
            catch (Exception e)
            { log.Error("ReadConfig {0}", e.Message); }
            return cfglines;
        }
        private List<CfgConvert> ReadConvertOption()
        {
            log.Trace("Читаем файлы настроек для конвертеров");
            List<CfgConvert> cfgconvertlines = new List<CfgConvert>();
            try
            {
                string[] files = Directory.GetFiles(appconfig.PathConvert, "*.cfg");
                log.Trace("Количество найденных файлов {0}", files.Length.ToString());
                if (files.Length == 0)
                    log.Trace("Не найден ни один конфигурационный файл для конвертера в каталоге {0}", appconfig.PathCfg);
                foreach (string file in files)
                {
                    try
                    {
                        log.Trace("Найден конфигурационный файл {0}", file);
                        string[] lines = File.ReadAllLines(file);
                        for (int i = 0; i < lines.Length; i++)
                        {
                            try
                            {
                                if (!String.IsNullOrEmpty(lines[i]) && !lines[i].Contains("#"))
                                {
                                    string[] buffer = lines[i].Split(';');
                                    cfgconvertlines.Add(new CfgConvert { name = buffer[0].Trim(), cmd = buffer[1].Trim(), outfile = buffer[2].Trim(), path_for_original = buffer.Count() > 3 ? buffer[3].Trim() : "" });
                                }
                            }
                            catch (Exception e)
                            { log.Error("ReadConvertOption {0} ошибка в строке {1} в файле {2}", e.Message,i,file); }
                        }
                    }
                    catch (Exception e)
                    { log.Error("ReadConvertOption {0} ошибка в файле", e.Message, file); }
                }
            }
            catch (Exception e)
            { log.Error("ReadConvertOption {0}", e.Message); }
            return cfgconvertlines;
        }
        /// <summary>
        /// Делаем имя для временной директории
        /// </summary>
        /// <param name="Input"></param>
        /// <param name="Sender"></param>
        /// <param name="Converter"></param>
        /// <returns></returns>
        private string CreateNameTempDirectory(string Memo, string Input, string Sender = "")
        {
            string RightName = Sender.Length > 100 ? Sender.Substring(1, 100) : Sender;
            try
            {
                if (Input.Contains("ftp://"))
                {
                    string[] buffer = Input.Split(':');
                    return PathSanitizer.SanitizePath(appconfig.PathTemp + Memo + "ftp." + buffer[1].Replace("//", "") + Input.Replace("/", "_").Replace(":", "_").Substring(Input.IndexOf("@")), '_');
                }
                if (Input.Contains("pop3"))
                {
                    string[] buffer = Input.Split(':');
                    return PathSanitizer.SanitizePath(appconfig.PathTemp + Memo + "pop3." + buffer[1] + "@" + buffer[3] + (!String.IsNullOrEmpty(RightName) ? ("." + RightName) : ""),'_');
                }
                return appconfig.PathTemp + Memo + Input.Replace("\\", "_").Replace(":", "_");
            }
            catch (Exception e)
            { log.Error("CreateNameTempDirectory {0}", e.Message); }
            return String.Empty;
        }

        /// <summary>
        /// Функция для получения списка файлов с ФТП 
        /// </summary>
        /// <returns> список файлов</returns>
        public List<string> GetFileList(string FtpString, Boolean isPassive = true)
        {   
            FtpWebRequest regFTP;
            List<string> buffer = new List<string>();
            try
            {

              //  string uri = GetFTPUri(FtpString);
              //  string username = GetFtpUserName(FtpString);
              //  string password = GetFtpPassword(FtpString);
                regFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(FtpString));
                regFTP.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                regFTP.Proxy = null;
                regFTP.UsePassive = isPassive;
                regFTP.ReadWriteTimeout = 50000;
                regFTP.Timeout = 50000;
                // regFTP.Credentials = new NetworkCredential(username, password);
                using (FtpWebResponse response = (FtpWebResponse)regFTP.GetResponse())
                {
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(responseStream))
                        {
                            while (!reader.EndOfStream)
                            {
                                string onlyfile = ParseFileName(reader.ReadLine());
                                if (!string.IsNullOrEmpty(onlyfile)) buffer.Add(onlyfile);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                if (CheckError(appconfig.PathTemp, new errorObject()
                {
                    errorProc = "GetFileList",
                    inputOrder = FtpString,
                    errorMessage = e.Message
                }))
                {
                    log.Error(" GetFileList: {0} {1}", FtpString, e.Message);
                }
                else
                {
                    log.Info(" SUPPRESS ERROR GetFileList: {0} {1}", FtpString, e.Message);
                } 

            }
                return buffer;
        }

        private string GetFtpPassword(string FtpString)
        {
            if (FtpString.IndexOf('@') == -1) return "anonymous";
            try
            {
                
                int IndexStartPassword = FtpString.IndexOf(':',6)+1; //6 ftp://
                int IndexEndPassword = FtpString.IndexOf('@');
                return FtpString.Substring(IndexStartPassword, IndexEndPassword -IndexStartPassword);
            }
            catch (Exception e)
            {
                log.Error("Фтп строка {0} скорее всего неправильна {1}", FtpString, e.Message);
            }
            return string.Empty;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="FtpString"></param>
        /// <returns></returns>
        private string GetFtpUserName(string FtpString)
        {
            if (FtpString.IndexOf('@') == -1) return "Anonymous";
            try
            {
                int IndexUserName = 6; // ftp://
                int IndexPassword = FtpString.IndexOf(':',6);
                return FtpString.Substring(IndexUserName, IndexPassword - IndexUserName);
            }
            catch (Exception e)
            {
                log.Error("Фтп строка {0} скорее всего неправильна {1}", FtpString, e.Message);
            }
            return string.Empty;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="FtpString"></param>
        /// <returns></returns>
        private string GetFTPUri(string FtpString)
        {   
           try
            {
                return FtpString.Replace("?passive=yes", string.Empty).Replace("?passive=no", string.Empty);
            }
            catch (Exception e)
            {
                log.Error("GetFTPUri : Фтп строка {0} скорее всего неправильна {1}", FtpString, e.Message);
            }
           return string.Empty;
        }
        /// <summary>
        /// ftp://user:pass@ftphost.tld/path/to/your/file.ext?passive=yes  Проверяем , нужно ли подключить пассивный режим
        /// </summary>
        /// <param name="FtpString"></param>
        /// <returns></returns>
        private Boolean isPassive(string FtpString)
        {
            try
            {
                if (FtpString.IndexOf("?passive=no") > 0)
                {
                    log.Trace("Переключаемся в активный режим {0} ", FtpString);
                    return false;
                }
            }
            catch (Exception e)
            {
                log.Error("isPassive: Фтп строка {0} скорее всего неправильна {1}", FtpString, e.Message);
            }
            return true;
        }                                                                                                                                    
        /// <summary>
        /// 
        /// </summary>
        /// <param name="NameFile">Полный путь к файлу с адресом логином паролем</param>
        /// <param name="outpath">выходной каталог для файла</param>
        /// <returns></returns>
        public bool DownloadDeleteFile(string FullPathFtp, string NameFile, CfgLine cfgline, Boolean isPassive = true)
        {
            FtpWebRequest regFTP;
            string outpath = cfgline.TempDirectory;
            string file_with_prefix = CreateNameFileWithPrefix(NameFile, cfgline);
            try
            {
              
                if (!Directory.Exists(outpath)) Directory.CreateDirectory(outpath);
                using (FileStream outputStream = new FileStream(Path.Combine(outpath, file_with_prefix), FileMode.Create))
                {
                    regFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(FullPathFtp + NameFile));
                    regFTP.Method = WebRequestMethods.Ftp.DownloadFile;
                    regFTP.UseBinary = true;
                    regFTP.Proxy = null;
                    regFTP.UsePassive = isPassive;
                    regFTP.ReadWriteTimeout = 50000;
                    using (FtpWebResponse response = (FtpWebResponse)regFTP.GetResponse())
                    {
                        using (Stream responseStream = response.GetResponseStream())
                        {
                            int bufferSize = 2048;
                            int readCount;
                            byte[] buffer = new byte[bufferSize];
                            readCount = responseStream.Read(buffer, 0, bufferSize);
                            while (readCount > 0)
                            {
                                outputStream.Write(buffer, 0, readCount);
                                readCount = responseStream.Read(buffer, 0, bufferSize);
                            }
                        }
                    }
                }

                if (File.Exists(Path.Combine(outpath, file_with_prefix)))
                {
                    try
                    {
                        regFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(FullPathFtp + NameFile));
                        regFTP.Method = WebRequestMethods.Ftp. DeleteFile;
                        regFTP.UseBinary = true;
                        regFTP.Proxy = null;
                        using (FtpWebResponse response = (FtpWebResponse)regFTP.GetResponse())
                        { };
                        //Вставлена проверка удаления файла 
                        if (!isFileExistsOnFtp(FullPathFtp, NameFile, isPassive))
                        {
                            log.Info("Файл {0} из {1} перемещен в {2}", NameFile, regFTP.RequestUri.Host, outpath);
                            return true;
                        }
                        else
                        {
                            if (File.Exists(Path.Combine(outpath, file_with_prefix))) File.Delete(Path.Combine(outpath, file_with_prefix));
                            log.Trace("Файл {0} из {1} не удалился на фтп , будет удален в {2} }", NameFile, regFTP.RequestUri.Host, outpath);
                        }
                    }
                     catch (Exception e)
                     {
                        if (File.Exists(Path.Combine(outpath, file_with_prefix))) File.Delete(Path.Combine(outpath, file_with_prefix));
                        if (CheckError(appconfig.PathTemp, new errorObject()
                                      {
                                       errorProc = "DownloadDeleteFile",
                                       inputOrder = cfgline.Input,
                                       errorMessage = e.Message
                                       }))

                        {
                            log.Error(" DownloadDeleteFile: Файл {0} из {1} перемещен {2} , но будет удален , при удалении на сервере возникла ошибка {3}", NameFile, regFTP.RequestUri.Host, outpath, e.Message);
                        }
                        else
                        {
                            log.Info(" SUPPRESS ERROR DownloadDeleteFile: Файл {0} из {1} перемещен {2} , но будет удален , при удалении на сервере возникла ошибка {3}", NameFile, regFTP.RequestUri.Host, outpath, e.Message);
                        }
                     }
                }
            }
            catch (Exception e)
            {
                if (File.Exists(Path.Combine(outpath, file_with_prefix)))
                {
                    log.Trace("Файл {0} из {1} принят с ошибкой, будет удален в {2} }", NameFile, FullPathFtp, outpath);
                    File.Delete(Path.Combine(outpath, file_with_prefix));
                }
                else
                log.Error(" DownloadDeleteFile: {0} {1}", FullPathFtp, e.Message);
            }
            return false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="files">Список файлов для отправки</param>
        /// <param name="FullFtpPath">Полный путь назначения для файлов включая фдрес логин и пароль</param>
        public static void UploadFile(string[] files, string FullFtpPath)
        {      
            foreach (var file in files)
            {
                try
                {
                    var regFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(FullFtpPath + Path.GetFileName(file)));
                    regFTP.Method = WebRequestMethods.Ftp.UploadFile;
                    regFTP.UseBinary = true;
                    regFTP.Proxy = null;
                    byte[] bytes = File.ReadAllBytes(file);
                    regFTP.ContentLength = bytes.Length;
                    var requestStream = regFTP.GetRequestStream();
                    requestStream.Write(bytes, 0, bytes.Length);
                    requestStream.Close();
                    FtpWebResponse response = (FtpWebResponse)regFTP.GetResponse();
                    if (response != null)
                        response.Close();
                    File.Delete(file);
                    log.Info("Файл {0} отправлен в {1}\\{2}", file, regFTP.RequestUri.Host,regFTP.RequestUri.AbsolutePath);
                }
                catch (Exception e)
                {
                    log.Error(" UploadFile {0} {1}", FullFtpPath, e.Message);
                }
            }
        }
        protected virtual bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                log.Trace("Файл {0} блокирован другим процессом", file.FullName);
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }
             
        Encoding CustomFallbackDecoder(string characterSet)
        {
            // Is it a "foo" encoding?
           // if (characterSet.StartsWith("foo"))
            //    return Encoding.ASCII; // then use ASCII

            // If no special encoding could be found, provide UTF8 as default.
            // You can also return null here, which would tell OpenPop that
            // no encoding could be found. This will then throw an exception.
            return Encoding.ASCII;
        }
        /// <summary>
        /// Возвращаем только имя файла
        /// </summary>
        /// <param name="Details">Детальная инфа с ФТП о файле</param>
        /// <returns></returns>
        public string ParseFileName (string Details)
        {
            try
            {
                if (Details.Contains("total")) { return string.Empty; };
                string[] buffer = Details.Split(new string[] { " ", }, StringSplitOptions.RemoveEmptyEntries);
                if (buffer.Length > 7) //Parse Unix Format
                {
                    bool isFile = buffer[0].Substring(0, 1) != "d";
                    if (isFile)
                    {
                        string name = string.Empty;
                        for (int i = 8; i < buffer.Length; i++)
                        {
                            name = string.Join(" ", name, buffer[i]);
                        }
                        return name.TrimStart();
                    }
                    else return string.Empty;
                }
                if (buffer.Length < 8) //Parse Microsoft format
                {
                    bool isFile = buffer[2] != "<DIR>";
                    if (isFile)
                    {
                        string name = string.Empty;
                        for (int i = 3; i < buffer.Length; i++)
                        {
                            name = string.Join(" ", name, buffer[i]);
                        }
                        return name.TrimStart();
                    }
                    else return string.Empty;
                }
                log.Error(" ParseFileName: Невозможно получить имя файла {0}", Details);
                
            }
            catch (Exception e)
            {
                log.Error(" ParseFileName: {0} {1}", Details, e.Message);
                throw new Exception(e.Message);
            }
            return string.Empty;
        }
    }
    /// <summary>
    /// Читаем app.config
    /// </summary>
    public class ReadAppConfig
    {
        public string PathCfg { get { return System.Configuration.ConfigurationManager.AppSettings["PathCfg"]; } }
        public string PathPricesCfg { get { return System.Configuration.ConfigurationManager.AppSettings["PathPricesCfg"]; } }
        public string PathTemp { get { return System.Configuration.ConfigurationManager.AppSettings["PathTemp"]; } }
        public string PathConvert { get { return System.Configuration.ConfigurationManager.AppSettings["PathConvert"]; } }
        public string TimeOut { get { return System.Configuration.ConfigurationManager.AppSettings["Timeout"]; } }
        public string StartTime { get { return System.Configuration.ConfigurationManager.AppSettings["StartTime"]; } }
        public string EndTime { get { return System.Configuration.ConfigurationManager.AppSettings["EndTime"]; } }
        public string MaxDayLog { get { return System.Configuration.ConfigurationManager.AppSettings["MaxDayLog"]; } }
        // интервал подавления ошибок. в течении этого интервала одинаковые ошибки будут тип info
        public string IntervalError { get { return System.Configuration.ConfigurationManager.AppSettings["IntervalError"]; } }
    }
    public class errorObject
    {
        public string errorProc { get; set; }
        public string inputOrder { get; set; }
        public string errorMessage { get; set; }
        public TimeSpan time { get; set; }
    }
   


}
