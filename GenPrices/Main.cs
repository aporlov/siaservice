﻿using GenPrices.FastDBF;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GenPrices
{
    class Program
    {
        public static Logger log;
        static int Main(string[] args)
        {
            try
            {
                log = LogManager.GetLogger("PriceLog");
                if (args.Length < 1)
                {
                    System.Console.WriteLine("Попытка запуска программы без параметров. Примеры правильных параметров: 1 (то есть день недели),08:30, email.cfg, *.cfg , id, ");
                }
                if (args[0].Length == 1)
                {
                    //Разберем правильность параметров
                    if (!CheckParameter(args[0]))
                    {
                        System.Console.WriteLine("Неправильный параметр.Примеры правильных параметров: 1 (то есть день недели),08:30, email.cfg, *.cfg , id");
                        return 1;
                    }
                }

                log.Trace("Стартуем {0} c параметрами {1}", DateTime.Now.ToString("dd.MM.yyyy HH:mm"), string.Join(" ",args));
                Prices proc = new Prices();
                proc.DoPrices(args);
                return 0;
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.Message);
            }
            return 1;
        }
        static bool CheckParameter(string Parameter)
        {
            if (new Regex(@"^\d+$").IsMatch(Parameter)) return true;
            if (Parameter.Contains(".cfg")) return true;
            if (Parameter.Contains(":") && Parameter.Length == 5) return true;

            return false;
        }
       
    }
    public class CfgPrice
    {
        public string name;
        public string pricefield;
        public string id;
        public string destination;
        public string cmd;
        public string outfile;
        public string dayofweek;
        public string time;
        public string arch;
        public string scheme;
        public string subjectLetter;
        public string bodyLetter;
    }
    public class Prices
    {
        public static string host;
        public static string port;
        public static string username;
        public static string password;
        public static string Sender;
        public static string Subject;
        public static Logger log;
        public List<CfgPrice> cfglines;
        public ReadAppConfig appconfig;
        public static Logger ReportLog; //Для отчета в отдел сбыта
        public static List<string> ReportForSbut = new List<string>(); // для отчета в отдел сбыта
        public static Dictionary<string, bool> MonitorSendingPrice = new Dictionary<string, bool>();
        public bool DoPrices(string[] Parameters)
        {
            try
            {
                SmtpSection mailSettings = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
                host = mailSettings.Network.Host;
                port = Convert.ToString(mailSettings.Network.Port);
                username = mailSettings.Network.UserName;
                password = mailSettings.Network.Password;
                Sender = ConfigurationManager.AppSettings["Sender"];
                Subject = ConfigurationManager.AppSettings["Subject"];
                log = LogManager.GetLogger("PriceLog");
                ReportLog = LogManager.GetLogger("ReportLog");
                log.Trace("Стартуем {0}", DateTime.Now.ToString("dd.MM.yyyy HH:mm"));
                log.Trace("Читаем конфиг");
                appconfig = new ReadAppConfig();
                if (String.IsNullOrEmpty(appconfig.PathPricesCfg))
                {
                    log.Error("Не определен каталог для конфигурации файлов");
                    return false;
                }
                if (String.IsNullOrEmpty(appconfig.PathAsk))
                {
                    log.Error("Не определен каталог для ask  файлов");
                    return false;
                }
                if (String.IsNullOrEmpty(appconfig.PathOut)) 
                {
                    log.Error("Не определен каталог для выходных файлов прайсов");
                    return false;
                }
                if (String.IsNullOrEmpty(appconfig.PathProgram)) 
                {
                    log.Error("Не определен каталог с программами генерации файлов и архиваторов");
                    return false;
                }
                log.Trace("PathPricesCfg : {0}", appconfig.PathPricesCfg);
                log.Trace("PathAsk : {0}", appconfig.PathAsk);
                log.Trace("PathOut : {0}", appconfig.PathOut);
                log.Trace("PathProgram : {0}", appconfig.PathProgram);

                //Если первый запуск создаем временную директорию
                if (!Directory.Exists(appconfig.PathOut)) Directory.CreateDirectory(appconfig.PathOut);
                cfglines = new List<CfgPrice>();
                foreach (string Parameter in Parameters)
                cfglines.AddRange(ReadConvertOption(Parameter));
        
                foreach (var cfgline in cfglines)
                {
                    //#Имя клиента (Ценовая колонка для мультиколоночного прайса);Ид из АСУ;Куда отправить,Программа с командной строкой.Если файл с расширением ask, то запуск asker.exe, если cnf то запуск genprice.exe:Имя файла прайс листа;Дни запуска;Время запуска:Если нужен архив,то указать rar или zip, 
                    log.Trace("Name:{0} Id:{1} Destination:{2} Command:{3} Out:{4} Day:{5} Time:{6} Arch{7} Pricefield {8}",
                        cfgline.name, cfgline.id, cfgline.destination, cfgline.cmd, cfgline.outfile, cfgline.dayofweek, cfgline.time,cfgline.arch,cfgline.pricefield);
                    if (!MonitorSendingPrice.ContainsKey(cfgline.name))
                    {
                        MonitorSendingPrice.Add(cfgline.name, true); //Отслеживаем отправку прайсов. Устанавливаем все прайсы в true, то есть с от правкой все ок
                    }
                }
                //Теперь вызываем программу котрая будет делать прайс и отсылаем файл
                #region Создание одиночных прайсов в цикле
                foreach (var cfgline in cfglines)
                {
                    try
                    {
                        if (!Execute(cfgline, appconfig))
                        { MonitorSendingPrice[cfgline.name] = false; }
                        //Если мультиколоночный прайс, то не отправляем и не архивируем . просто делаем файлы на каждого клиента,потом приступим к созданию
                        if (!String.IsNullOrEmpty(cfgline.pricefield)) continue;
                        //Если нужно заархивировать
                        if (!String.IsNullOrEmpty(cfgline.arch))
                        {
                          if (!CreateSendArchive(cfgline))
                             { MonitorSendingPrice[cfgline.name] = false; }
                        }
                        else
                        {
                            if (File.Exists(Path.Combine(appconfig.PathOut, cfgline.name, cfgline.outfile)) && CheckDate(Path.Combine(appconfig.PathOut, cfgline.name, cfgline.outfile)))
                            {
                                if (!SendPrice(Path.Combine(appconfig.PathOut, cfgline.name, cfgline.outfile), cfgline))
                                     { MonitorSendingPrice[cfgline.name] = false; }
                            }
                            else
                            {
                                log.Error(String.Format("DoPrice. Нет файла для отправки {0}", Path.Combine(appconfig.PathOut, cfgline.outfile)));
                                MonitorSendingPrice[cfgline.name] = false;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        {
                            log.Error(String.Format("DoPrice: Ошибка при обработке конфига {0} {1}", cfgline.name, e.Message));
                        }
                    }
                }
                #endregion
                #region Создание мультиколоночного прайса
                //все прайсы сделаны и отправлены кроме мультиколоночного теперь создаем
                foreach (var itemGroup in cfglines.Where(c => !String.IsNullOrEmpty(c.pricefield)).GroupBy(x => x.cmd))
                {
                    List<CfgPrice> multicolumnpricelines = cfglines.Where(c => c.cmd == itemGroup.Key).ToList();
                    if (!MonitorSendingPrice.ContainsKey(multicolumnpricelines.Last().outfile))
                    {
                        MonitorSendingPrice.Add(multicolumnpricelines.Last().outfile, true);
                        if (!GeneratePriceMultiDBF(multicolumnpricelines))
                        {
                            MonitorSendingPrice[multicolumnpricelines.Last().outfile] = false;
                            return false;
                        };
                    }
                    string pathtoffile = CreateMultiNamePrice(multicolumnpricelines.First());
                    string nameoffile = multicolumnpricelines.First().outfile;
                    string fullnameoffile = Path.Combine(appconfig.PathOut, pathtoffile, nameoffile);
                    CfgPrice destinationCfg = multicolumnpricelines.First(c => !String.IsNullOrEmpty(c.destination));

                    if (multicolumnpricelines.First(c => !String.IsNullOrEmpty(c.arch)) != null)
                    {

                       if ( !CreateSendArchive(multicolumnpricelines.First(c => !String.IsNullOrEmpty(c.arch)), fullnameoffile))
                        { MonitorSendingPrice[multicolumnpricelines.Last().outfile] = false; }

                    }
                    else
                    {
                        if (File.Exists(fullnameoffile) && CheckDate(fullnameoffile))
                        {
                            if (!SendPrice(fullnameoffile, destinationCfg))
                            { MonitorSendingPrice[multicolumnpricelines.Last().outfile] = false; }
                        }
                        else
                        {
                            log.Error(String.Format("DoPrice. Нет файла для отправки {0}", Path.Combine(fullnameoffile)));
                            MonitorSendingPrice[multicolumnpricelines.Last().outfile] = false;
                        }

                    }
                }
                #endregion
                ReportLog.Info(MakeInfo(MonitorSendingPrice, string.Join(" ", Parameters)));
                return true;
            }
            catch (Exception e)
            {
                log.Error(String.Format("Main: {0}", e.Message));
                System.IO.File.AppendAllText("c:\\temp\\logs.log", e.Message);
            }
            return false;
        }

        public string MakeInfo(Dictionary<string, bool> monitorSendingPrice, string v)
        {
            string rt = "Ошибка создания отчета";
            try
            {
                ReportForSbut.Add("<p>");
                ReportForSbut.Add(String.Format(" {0}: Стартуем c параметрами {1} <br/>", DateTime.Now.ToString("dd.MM.yyyy HH:mm"), string.Join(" ", v)));
                foreach (var item in monitorSendingPrice)
                {
                    if (item.Value) ReportForSbut.Add(String.Format("{0} - Ok", item.Key));
                    else ReportForSbut.Add(String.Format("{0} - Error", item.Key));
                }
                rt = string.Join("<br/>", ReportForSbut.ToArray());
                ReportForSbut.Add("</p>");
            }
            catch (Exception e)
            {
                log.Error(String.Format("MakeInfo: {0}", e.Message));
            }
            return rt;
        }

        public bool CreateSendArchive(CfgPrice cfgline,string fullnameoffile = "")
        {
            bool rt = true;
            string arch_program = String.Empty;
            string argument = String.Empty;
            string outfile = String.Empty;
            string outarch = String.Empty;
            string fileOrExt = cfgline.arch.Contains(".") ? cfgline.arch : Path.GetFileNameWithoutExtension(cfgline.outfile) + "." + cfgline.arch;
            try
            {
                if (String.IsNullOrEmpty(fullnameoffile))
                {
                    outarch = Path.Combine(appconfig.PathOut, cfgline.name,   fileOrExt);
                    outfile = Path.Combine(appconfig.PathOut, cfgline.name, cfgline.outfile);
                }
                else
                {
                    outfile = fullnameoffile;
                    outarch = Path.Combine(Path.GetDirectoryName(fullnameoffile), fileOrExt);
                }
                //Удаляем на всякий случай старый архив . Если destination не указан , то оставляем
                if (!string.IsNullOrEmpty(cfgline.destination)&& File.Exists(outarch) && !IsFileLocked(new FileInfo(outarch))) File.Delete(outarch);

                if (cfgline.arch.Contains("rar"))
                {
                    if (appconfig.rar.IndexOf('\"') != -1)
                    {
                        argument = appconfig.rar.Substring(appconfig.rar.IndexOf(' ') + 1)
                                     .Replace("%arch", outarch)
                                     .Replace("%file", outfile);
                    }
                    else
                    {
                        argument = appconfig.rar.Substring(appconfig.rar.IndexOf(' ') + 1)
                                     .Replace("%arch", outarch)
                                     .Replace("%file", outfile);

                    }

                    arch_program = appconfig.rar.Substring(0, appconfig.rar.IndexOf(' '));
                }
                if (cfgline.arch.Contains("zip"))
                {
                    argument = appconfig.zip.Substring(appconfig.zip.IndexOf(' ') + 1)
                                .Replace("%arch", outarch)
                                .Replace("%file", outfile);

                    arch_program = appconfig.zip.Substring(0, appconfig.zip.IndexOf(' '));
                }
                //Новая функция конвератция ДБФ в CSV а потом еще и заархивируем
                if (cfgline.arch == "csv")
                {
                    log.Info(String.Format("CreateSendArchive: Запускаем конвертер dfb2csv "));
                     string csv_argument = appconfig.csv.Substring(appconfig.csv.IndexOf(' ') + 1)
                                .Replace("%out", outfile.Replace("dbf","csv"))
                                .Replace("%in", outfile);
                     string csv_program = appconfig.csv.Substring(0, appconfig.csv.IndexOf(' '));
                     if (File.Exists(csv_program))
                         RunProgram(csv_program, csv_argument);
                     else
                         log.Error(String.Format("CreateSendArchive: Не найдена программа архиватор  {0}", arch_program));
                     outfile = outfile.Replace("dbf", "csv");
                     outarch = outarch.Replace("csv", "zip");
                    argument = appconfig.zip.Substring(appconfig.zip.IndexOf(' ') + 1)
                                 .Replace("%arch", outarch)
                                 .Replace("%file", outfile);

                    arch_program = appconfig.zip.Substring(0, appconfig.zip.IndexOf(' '));
                }
                if (String.IsNullOrEmpty(arch_program))
                {
                    log.Error(String.Format("CreateSendArchive: Неопределен архиватор. Исправьте строку в config файле {0}", cfgline.name));
                    return false;
                }

                if (File.Exists(outfile) && CheckDate(outfile))
                {
                    if (File.Exists(arch_program))
                        RunProgram(arch_program, argument);
                    else
                    {
                        log.Error(String.Format("CreateSendArchive: Не найдена программа архиватор  {0}", arch_program));
                        rt = false;
                    }
                }
                else
                {
                    log.Error(String.Format("CreateSendArchive: Нет файла для отправки {0}", outarch));
                    rt = false;
                }
                //Если существует архив то отправляем 
                if (File.Exists(outarch)
                    && CheckDate(outarch))
                {
                   return SendPrice(outarch, cfgline);
                }
                else
                {
                    log.Error(String.Format("CreateSendArchive: Нет файла для отправки {0}", outarch));
                    rt = false;
                }
            }
            catch (Exception e)
            {
                log.Error(String.Format("CreateSendArchive: ошибка файла {0}  {1}", outarch,e.Message));
                rt = false;
            }
            return rt;
        }
        /// <summary>
        /// Генерация мультиколоночного прайса
        /// </summary>
        /// <param name="enumerable"></param>
        private bool GeneratePriceMultiDBF(IEnumerable<CfgPrice> cfglines)
        {
            try
            {
                string pathTofile = CreateMultiNamePrice(cfglines.First());
                string namefile = cfglines.First().outfile;
                DataTable cs = new DataTable();
                int i = 0;
                List<DataTable> tables = new List<DataTable>();
                int LastColumnforAdd = 0;
                DataTable TblUnion;
                foreach (var cfgline in cfglines)
                {
                    try
                    {
                        if (File.Exists(Path.Combine(appconfig.PathOut, cfgline.name, cfgline.outfile)))
                        {
                           // DataTable buffer = ReadDBF(Path.Combine(appconfig.PathOut, cfgline.name, cfgline.outfile));
                            //Попробую новую версию, неиспользующую провайдера ДБФ 
                            DataTable buffer = ReadDBFNoProvider(Path.Combine(appconfig.PathOut, cfgline.name, cfgline.outfile));
                            if (LastColumnforAdd == 0) LastColumnforAdd = buffer.Columns.Count;
                            tables.Add(ReadDBFNoProvider(Path.Combine(appconfig.PathOut, cfgline.name, cfgline.outfile)));
                        }
                        else
                        {
                            log.Error(String.Format("GeneratePriceMultiDBF Нет входного файла для создания прайса {0}", Path.Combine(appconfig.PathOut, cfgline.name, cfgline.outfile))); 
                            return false;
                        }
                        if (tables.Count >3)
                        {
                            log.Trace(String.Format("GeneratePriceMultiDBF Делаю объединении по ключу {0} ", tables[0].Columns[0].ColumnName));
                            TblUnion = tables.MergeAll(tables[0].Columns[0].ColumnName);
                            tables.RemoveAll(c => 1 == 1);
                            tables.Add(TblUnion);
                        }
                    }
                    catch (Exception e)
                    { log.Error(String.Format("GeneratePriceMultiDBF Ошибка файла {0} {1}",Path.Combine(appconfig.PathOut, cfgline.name, cfgline.outfile), e.Message)); }
                }
                
                TblUnion = tables.MergeAll(tables[0].Columns[0].ColumnName);
                DataSet ds = new DataSet();
                ds.Tables.Add(TblUnion);
                log.Trace(String.Format("GeneratePriceMultiDBF Вызов процедуры Экспорта в файл Export. Число Колонок {0}",ds.Tables[0].Columns.Count));
               return  ExportDBFNewVersion(ds, Path.Combine(appconfig.PathOut, pathTofile, namefile), Path.Combine(appconfig.PathOut, cfglines.First().name, cfglines.First().outfile), LastColumnforAdd);
            }
            catch (Exception e)
            {
                log.Error(String.Format("GeneratePriceMultiDBF {0}", e.Message));
            }
            return false;
        }
         
        private string CreateMultiNamePrice(CfgPrice cfgPrice)
        {
            return cfgPrice.cmd.Replace(" ", string.Empty)
                                .Replace("%id", string.Empty)
                                .Replace("/", string.Empty)
                                .Replace(":", string.Empty)
                                .Replace("*", string.Empty)
                                .Replace("?", string.Empty)
                                .Replace("\"", string.Empty);
        }
        
        public DataTable ReadDBF(string DB_path)
        {
            #region
            ////OleDbConnection Conn = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + DB_path + "; Extended Properties=DBASE III;");
            //OleDbConnection Conn = new OleDbConnection("Provider=VFPOLEDB; Data Source= " + DB_path + "; Extended Properties=DBASE III;");
            //string Command = ("SELECT * FROM " + DB_path);
            //DataTable dt = null;
            //if (Conn != null)
            //{
            //    try
            //    {
            //        Conn.Open();
            //        OleDbCommand cmd = new OleDbCommand(Command, Conn);
            //        OleDbDataReader dr1 = cmd.ExecuteReader();
            //        if (dr1.HasRows)
            //        {
            //            dt = new DataTable();
            //            dt.Load(dr1);
            //        }
            //        Conn.Close();
            //    }
            //    catch (Exception e)
            //    {
            //        log.Error(String.Format("ReadDBF {0}",e.Message));
            //    }
            //}
            #endregion
            DataTable dsFill = new DataTable();
            try
            {
                string connString = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + Path.GetDirectoryName(DB_path) + "; Extended Properties=DBASE III;";
                string createStatement = "Select * from " + Path.GetFileNameWithoutExtension(DB_path);
                OleDbCommand cmd = new OleDbCommand();
                using(OleDbConnection conn = new OleDbConnection(connString))
                {
                DataSet dsCreateTable = new DataSet();
                //Open the connection
                 conn.Open();
                //Create the DBF table          
                 OleDbDataAdapter daInsertTable = new OleDbDataAdapter(createStatement, conn);
                 daInsertTable.Fill(dsFill);
                 conn.Close();
                }
            }
            catch (Exception e)
            {
                log.Error(String.Format("ReadDBF: {0}", e.Message));
            }

            return dsFill;
        }
        public DataTable ReadDBFNoProvider(string DB_path)
        {
            DataTable datatable = new DataTable();
            int codepage = GetDbfCodepage(DB_path);
            bool anyPriceExists = false;
            DbfFile dbf = new DbfFile(Encoding.GetEncoding(codepage));
            try
            {
                dbf.Open(DB_path, FileMode.Open);
                log.Trace(String.Format("ReadDBFNoProvide: количество строк в файле {0} = {1}", DB_path, dbf.Header.RecordCount));
                // Создаем структуру дататабл
                for (int i = 0; i < dbf.Header.ColumnCount; i++)
                {
                    switch (dbf.Header[i].ColumnType)
                    {
                        case DbfColumn.DbfColumnType.Character: datatable.Columns.Add(dbf.Header[i].Name, typeof(String)); ;
                            break;
                        case DbfColumn.DbfColumnType.Integer: datatable.Columns.Add(dbf.Header[i].Name, typeof(Int16));
                            break;
                        case DbfColumn.DbfColumnType.Number: datatable.Columns.Add(dbf.Header[i].Name, typeof(double));
                            break;
                        case DbfColumn.DbfColumnType.Date: datatable.Columns.Add(dbf.Header[i].Name, typeof(DateTime));
                            break;
                        case DbfColumn.DbfColumnType.Boolean: datatable.Columns.Add(dbf.Header[i].Name, typeof(Boolean));
                            break;
                    }
                }
                if (datatable.Columns.Count != dbf.Header.ColumnCount)
                {
                    throw new Exception("Количество колонок не совпадает");
                }

                //output values for all but binary and memo...
                DbfRecord orec = new DbfRecord(dbf.Header);
                for (int j = 0; j < dbf.Header.RecordCount; j++)
                {
                    if (!dbf.Read(j, orec))
                        break;
                    DataRow datarow = datatable.NewRow();

                    //output column values...
                    if (!orec.IsDeleted)
                    {
                        for (int i = 0; i < orec.ColumnCount; i++)
                        {
                            
                            if (orec.Column(i).ColumnType != DbfColumn.DbfColumnType.Memo ||
                                orec.Column(i).ColumnType != DbfColumn.DbfColumnType.Binary)
                                if (orec.Column(i).ColumnType == DbfColumn.DbfColumnType.Character)
                                    datarow[i] = orec[i];
                                else if (orec.Column(i).ColumnType == DbfColumn.DbfColumnType.Integer)
                                {
                                    int inte;
                                    if (Int32.TryParse(orec[i], out inte))
                                    {
                                        datarow[i] = inte;
                                    }
                                }
                                else if (orec.Column(i).ColumnType == DbfColumn.DbfColumnType.Number)
                                {       
                                    double number;
                                    if (Double.TryParse((orec[i].Replace(",", ".")), NumberStyles.Any, CultureInfo.InvariantCulture, out number))
                                    {
                                        if (number > 0 && i == orec.ColumnCount - 1 ) {
                                            anyPriceExists = true;
                                        }
                                        datarow[i] = number;
                                    }
                                }
                                else if (orec.Column(i).ColumnType == DbfColumn.DbfColumnType.Date)
                                {
                                    DateTime datetime;
                                    if (DateTime.TryParseExact(orec[i], "yyyymmdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out datetime))
                                    {

                                        datarow[i] = datetime;
                                    }
                                }
                        }
                        datatable.Rows.Add(datarow); 
                    }

                }
             if (!anyPriceExists) { log.Trace(String.Format("ReadDBFNoProvider: Прайс без цен {0} G", DB_path)); }
                
            }
            catch(Exception e)
            {
                log.Error(String.Format("ReadDBFNoProvider: {0}", e.Message));
            }
            finally
            {
                dbf.Close();
            }
            return datatable;
        }
        public void DeleteDBF(string DB_path)
        {
            ParseDBF.ReadDBF(DB_path);
            string connString = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + Path.GetDirectoryName(DB_path) + "; Extended Properties=DBASE III;";
            string deleteStatement = "Delete * from " + Path.GetFileNameWithoutExtension(DB_path);
            OleDbCommand cmd = new OleDbCommand();
            using (OleDbConnection conn = new OleDbConnection(connString))
            {
                conn.Open();
                OleDbDataAdapter oledbAdapter = new OleDbDataAdapter();
                oledbAdapter.DeleteCommand = conn.CreateCommand();
                oledbAdapter.DeleteCommand.CommandText = deleteStatement;
                oledbAdapter.DeleteCommand.ExecuteNonQuery();
                conn.Close();
            }
        }
        /// <summary>
        /// Создание дбф файла
        /// </summary>
        /// <param name="dsExport"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public bool ExportDBF(DataSet dsExport, string filePath, string filepathforstructure, int LastColumnForAdd)
        {
            bool returnStatus = false;
            try
            {
                string tableName = Path.GetFileNameWithoutExtension(filePath);
                string folderPath = Path.GetDirectoryName(filePath);
                if (!Directory.Exists(folderPath)) Directory.CreateDirectory(folderPath);
                if (System.IO.File.Exists(filePath)) System.IO.File.Delete(filePath);
                if (File.Exists(filepathforstructure)) File.Copy(filepathforstructure, filePath);
                //Удаляем все строки из ДБФ файла и используем его как основу добавляя недостоющие колонки
                ParseDBF.DeleteRowsDBF(filePath);
                // This function give the Folder name and table name to use in
                // the connection string and create table statement.

                // here you can use DBASE IV also
                string connString = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + folderPath + "; Extended Properties=DBASE III;";
                string insertStatement = "Insert Into " + tableName + " Values ( ";
                string insertTemp = string.Empty;
                //OleDbCommand cmd = new OleDbCommand();
                //OleDbConnection conn = new OleDbConnection(connString);
                // This for loop to create "Create table statement" for DBF
                // Here I am creating varchar(250) datatype for all column.
                // for formatting If you don't have to format data before
                // export then you can make a clone of dsExport data and transfer // data in to that no need to add datatable, datarow and
                // datacolumn in the code.
                for (int iCol = LastColumnForAdd; iCol < dsExport.Tables[0].Columns.Count; iCol++)
                {
                    using (OleDbConnection conn1 = new OleDbConnection(connString))
                    {
                        conn1.Open();
                        string columnname = dsExport.Tables[0].Columns[iCol].ColumnName.ToString();
                        string createStatement = "ALTER TABLE " + tableName + " ADD COLUMN " + columnname + " NUMERIC(20,4)";
                        using (OleDbDataAdapter oledbAdapter = new OleDbDataAdapter())
                        {
                            oledbAdapter.SelectCommand = conn1.CreateCommand();
                            oledbAdapter.SelectCommand.CommandText = createStatement;
                            oledbAdapter.SelectCommand.ExecuteNonQuery();
                        }
                        conn1.Close();
                    }
                }
                using (OleDbConnection conninsert = new OleDbConnection(connString))
                {
                    conninsert.Open();
                using (OleDbDataAdapter oledbAdapter = new OleDbDataAdapter())
                {
                    oledbAdapter.InsertCommand = conninsert.CreateCommand();
                for (int row = 0; row < dsExport.Tables[0].Rows.Count; row++)
                {
                    insertTemp = insertStatement;
                    
                    for (int col = 0; col < dsExport.Tables[0].Columns.Count; col++)
                    {
                        if (col == dsExport.Tables[0].Columns.Count - 1)
                        {
                            if (dsExport.Tables[0].Columns[col].DataType.Name.Equals("Decimal") ||
                                dsExport.Tables[0].Columns[col].DataType.Name.Equals("Double")
                                )
                                insertTemp += string.IsNullOrEmpty(dsExport.Tables[0].Rows[row][col].ToString()) ? "null);" : Convert.ToDecimal(dsExport.Tables[0].Rows[row][col]).ToString("0.00", CultureInfo.InvariantCulture) + ") ;";
                            else
                                if (dsExport.Tables[0].Columns[col].DataType.Name.Contains("Int"))
                                    insertTemp += dsExport.Tables[0].Rows[row][col] + ") ;";
                                else
                                    insertTemp += "'" + dsExport.Tables[0].Rows[row][col].ToString().Replace("'","''") + "' ) ;";
                        }
                        else
                        {
                            if (dsExport.Tables[0].Columns[col].DataType.Name.Equals("Decimal") ||
                                dsExport.Tables[0].Columns[col].DataType.Name.Equals("Double"))
                                insertTemp += string.IsNullOrEmpty(dsExport.Tables[0].Rows[row][col].ToString()) ? "null," : Convert.ToDecimal(dsExport.Tables[0].Rows[row][col]).ToString("0.00",CultureInfo.InvariantCulture) + ",";
                            else
                                if (dsExport.Tables[0].Columns[col].DataType.Name.Contains("Int"))
                                    insertTemp += dsExport.Tables[0].Rows[row][col] + ",";
                                else
                                    insertTemp += "'" + dsExport.Tables[0].Rows[row][col].ToString().Replace("'","''")  + "' , ";
                        }
                    }
                   
                    //This lines of code insert Row One by one to above created
                    //datatable.
                    
                           
                            oledbAdapter.InsertCommand.CommandText = insertTemp;
                            oledbAdapter.InsertCommand.ExecuteNonQuery();
                        
                } // close outer for loop
                    }
                conninsert.Close();
                    //Установка кодовой страницы MSDOS в дбф файле; 
                SetDbfCodepage(filePath,866);
                }
                returnStatus = true;
            }
            catch (Exception e)
            {
                log.Error(String.Format("ExportDBF Создание дбф {0} не получилось. Ошибка {1}",filePath, e.Message));
            }
            return returnStatus;
        }
        public bool ExportDBFNewVersion(DataSet dsExport, string filePath, string filepathforstructure, int LastColumnForAdd)
        {
            bool returnStatus = false;
            bool anyPriceExists = false;
            try
            {
                string tableName = Path.GetFileNameWithoutExtension(filePath);
                string folderPath = Path.GetDirectoryName(filePath);
                string bufferfilename = Path.Combine(folderPath, tableName + "_temp.dbf");
                if (!Directory.Exists(folderPath)) Directory.CreateDirectory(folderPath);
                if (System.IO.File.Exists(filePath)) System.IO.File.Delete(filePath);
                if (File.Exists(filepathforstructure)) File.Copy(filepathforstructure, filePath);
                   
                //Удаляем все строки из ДБФ файла и используем его как основу добавляя недостающие колонки
                //ParseDBF.DeleteRowsDBF(filePath);
                int coding = GetDbfCodepage(filePath);
                var odbf = new DbfFile(Encoding.GetEncoding(coding));
                odbf.Open(filePath, FileMode.Open);
                
                var rec = new DbfRecord(odbf.Header);
                int LengthColumn = rec.Column(rec.ColumnCount - 1).Length;
                int DecimalCount = rec.Column(rec.ColumnCount - 1).DecimalCount;
                int count = 0; //Счетчик колонок

                var bufferfile = new DbfFile(Encoding.GetEncoding(coding));
                bufferfile.Open(bufferfilename,FileMode.Create);
               // bufferfile.Header.Unlock();
                for (int i=0; i < rec.ColumnCount; i++)
                {
                    bufferfile.Header.AddColumn(new DbfColumn(rec.Column(i).Name,rec.Column(i).ColumnType,rec.Column(i).Length,rec.Column(i).DecimalCount));
                }
                foreach (DataColumn Column in dsExport.Tables[0].Columns)
                {
                    if ( count >= LastColumnForAdd)
                        bufferfile.Header.AddColumn(new DbfColumn(Column.ColumnName, DbfColumn.DbfColumnType.Number, LengthColumn, DecimalCount));
                    count++;
                }
                odbf.Close();
                bufferfile.Close();
                bufferfile.Open(bufferfilename, FileMode.Open);
                var orec = new DbfRecord(bufferfile.Header) { AllowDecimalTruncate = true };             
                foreach (DataRow row in dsExport.Tables[0].Rows)
                {
                    for (int i = 0; i < orec.ColumnCount; i++)
                    {
                        string buffer = "";
                        if (orec.Column(i).ColumnType==DbfColumn.DbfColumnType.Number)
                            buffer = row[i].ToString().Replace(',', '.');
                        else  buffer = row[i].ToString();

                        if (buffer.Length > 0 && i >= LastColumnForAdd) { anyPriceExists = true; }
                        orec[i] = buffer;
                    }
                    bufferfile.Write(orec, true);
                }
                bufferfile.Close();
                SetDbfCodepage(bufferfilename, coding);
                if (File.Exists(filePath))          
                    File.Delete(filePath);
                if (File.Exists(bufferfilename)) File.Move(bufferfilename, filePath);


                //Установка кодовой страницы MSDOS в дбф файле; 
                //    SetDbfCodepage(filePath);      
                if (!anyPriceExists) {
                    log.Error(String.Format("ExportDBF: Создание дбф {0} ошибка. Все цены нулевые", filePath));
                    returnStatus = false;
                } 
                returnStatus = true;
            }
            catch (Exception e)
            {
                log.Error(String.Format("ExportDBF: Создание дбф {0} не получилось. Ошибка {1}", filePath, e.Message));
            }
            return returnStatus;
        }
        //Установка кодовой страницы MSDOS в дбф файле; 

        private void SetDbfCodepage(string filePath,int coding)
        {
            // case 866: cp = 0x26; break; // Russian DOS
            // case 1251: cp = 0x87; break; // Russian Windows
            byte cp = 0x65;
            if (coding == 1251) cp = 0x87;
             
                 
               
            using (FileStream file = File.Open(filePath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
                try
                {
                    file.Seek(29, SeekOrigin.Begin); // 29 байт содержит значение кодовой страницы
                    file.WriteByte(cp);
                }
                catch (Exception ex)
                {
                    log.Error(String.Format("SetDbfCodepage: Ошибка изменения кодовой страницы: файл {0}, {1} ", filePath, ex.Message));
                }
            }
        }
        private void SetRightTerminalByte(string filePath)
        {
            // case 866: cp = 0x26; break; // Russian DOS
            // case 1251: cp = 0x87; break; // Russian Windows
            byte cp;
            cp = 0x20;

            using (FileStream file = File.Open(filePath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
                try
                {
                    file.Seek(29, SeekOrigin.Begin); // 29 байт содержит значение кодовой страницы
                    file.WriteByte(cp);
                }
                catch (Exception ex)
                {
                    log.Error(String.Format("SetDbfCodepage: Ошибка изменения кодовой страницы: файл {0}, {1} ", filePath, ex.Message));
                }
            }
        }
        /// <summary>
        /// Проверка байта кодовой страницы таблицы
        /// </summary>
        /// <param name="fileName">Имя файла</param>
        /// <returns>Байт-код кодовой страницы таблицы</returns>
        private int GetDbfCodepage(string filePath)
        {
            // case 866: cp = 0x26; break; // Russian DOS
            // case 1251: cp = 0x87; break; // Russian Windows
            byte cp = 0;
            using (FileStream file = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                try
                {
                    file.Seek(29, SeekOrigin.Begin); // 29 байт содержит значение кодовой страницы
                    cp = Convert.ToByte(file.ReadByte());
                }
                catch (Exception ex)
                {
                    log.Error(String.Format("GetDbfCodepage: Ошибка изменения кодовой страницы: файл {0}, {1} ", filePath, ex.Message));
                }
            }
            if (cp == 0x65 || cp == 0x26) return 866;
            return 1251;
        }
        //Проверка датф прайса старый не отправим
        private bool CheckDate(string file)
        {
            try
            {
                if (File.GetLastWriteTime(file).Date == DateTime.Today.Date) return true;
            }
            catch (Exception e)
            {
                log.Error(String.Format("CheckDate: {0}", e.Message));
            }
            return false;
        }
        /// <summary>
        /// Проверка , является ли destination фтп или адресом электронной почты
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static bool isMultiDestination(string p)
        {
            try
            {
                List<string> buff = p.Split(',').ToList();
                if (buff.Count() < 2) return false;
                if (buff.Where(c => c.StartsWith("ftp:")).Count() > 0) return true;
            }
            catch (Exception e)
            {
                log.Error("isMultiDestination():  {0} ", e.Message);
                throw new Exception(e.Message);
            }
            return false;
        }
        /// <summary>
        /// Проверка это фтп?
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private static bool isFtp(string p)
        {
            if (p.ToLower().IndexOf("ftp:") != -1) return true;
            return false;
        }

        private bool SendPrice(string namefile, CfgPrice cfgline)
        {
            bool rt = true;
            if (string.IsNullOrEmpty(cfgline.destination))
            {
                log.Error(string.Format(" Файл {0} не будет отправлен, не указан получатель {1}", namefile, cfgline.name));
                return false;
            }
            if (isMultiDestination(cfgline.destination))
            {
                #region Multidestination
                string[] buff = cfgline.destination.Split(',');
                //Внутри могут быть почтовые адреса поэтому 
                if (buff.Length > 1)
                {
                    for (int i = 0; i < buff.Length; i++)
                    {
                        CfgPrice newline = new CfgPrice()
                        {
                            bodyLetter = cfgline.bodyLetter,
                            destination = buff[i],
                            outfile = cfgline.outfile,
                            subjectLetter = cfgline.subjectLetter
                        };
                        if (isFtp(newline.destination)) rt = PutFTP(newline.destination, namefile) && rt;
                        else rt = SendEMAIL(newline, namefile) && rt;
                    }
                }
                return rt;
                #endregion
            }
            else
            {

                if (cfgline.destination.IndexOf("ftp:") != -1)
                {
                    return PutFTP(cfgline.destination, namefile);
                }
                if (cfgline.destination.IndexOf("\\") != -1)
                {
                    return PutIn(cfgline.destination, namefile);
                }
                if (cfgline.destination.IndexOf("ftp:") == -1)
                {
                    return SendEMAIL(cfgline, namefile);
                    
                }
            }
            log.Error(string.Format(" Файл {0} не был отправлен, не правильно получатель {1}", namefile, cfgline.name));
            return false;
        }
        //Если пункт назначения папка
        private bool PutIn(string destination, string namefile)
        {
            bool rt = true;
            if (CheckDate(namefile))
            {
                File.Copy(namefile, Path.Combine(destination, Path.GetFileName(namefile)), true);
                log.Info("Файл {0} отправлен в {1}", namefile, Path.Combine(destination, Path.GetFileName(namefile)));
            }
            else
            {
                log.Error("Файл {0} не будет отправлен в в {1} он устарел", namefile, Path.Combine(destination, Path.GetFileName(namefile)));
                rt = false;
            }
            return rt;
        }

            /// <summary>
        /// Отправка на FTP
        /// </summary>
        /// <param name="line"></param>
        /// <param name="files">Список файлов</param>
        private static bool PutFTP(string destination , string file)
        {
            bool rt = true;
            FtpWebRequest regFTP = null;
            FtpWebResponse response = null;
            try
            {
                regFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(destination + System.IO.Path.GetFileName(file)));
                regFTP.Method = WebRequestMethods.Ftp.UploadFile;
                regFTP.UseBinary = true;
                regFTP.Proxy = null;
                byte[] bytes = File.ReadAllBytes(file);
                regFTP.ContentLength = bytes.Length;
                var requestStream = regFTP.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
                response = (FtpWebResponse)regFTP.GetResponse();
                if (response != null)
                    response.Close();
                log.Info(string.Format("{0}: Файл {1} отправлен на ftp: {2}", DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss"), file, destination));
                
            }
            catch (Exception ex)
            {
                log.Error(string.Format("{0}: Error: Файл {1} {2} {3}", DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss"), file, destination, ex.Message));
                rt = false;    
            }
            return rt;
        }

        /// <summary>
        /// Отправить почту
        /// </summary>
        /// <param name="line"></param>
        /// <param name="files">Список файлов</param>
        private static bool SendEMAIL(CfgPrice cfgline, string file)
        {
            bool rt = true;
            string subject = string.IsNullOrEmpty(cfgline.subjectLetter) ? Subject : cfgline.subjectLetter;
            string body = string.IsNullOrEmpty(cfgline.bodyLetter) ? "" : cfgline.bodyLetter;
            try
                {
                   
                    System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage(
                       Sender,
                       cfgline.destination,
                       subject.Replace("%date", DateTime.Today.Date.ToString("dd.MM.yyyy")),
                       body);
                    Attachment attachment = new Attachment(file);
                    message.Attachments.Add(attachment);
                    using (System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient())
                    {
                        client.Send(message);
                    }
                    attachment = null;
                    message = null;
                    log.Info(string.Format("{0}: Файл {1} отправлен на почту: {2}", DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss"), file, cfgline.destination));
                }
                catch (Exception ex)
                {
                    log.Error(string.Format("SendEmail: Файл {0} {1} {2}",  file, cfgline.destination, ex.Message));
                    rt = false;
                }
            return rt;          
        }
/// <summary>
/// Запуск генерации прайса
/// </summary>
/// <param name="cfgline"></param>
/// <param name="appconfig"></param>
/// <returns></returns>
        private bool Execute(CfgPrice cfgline, ReadAppConfig appconfig)
        {
            bool rt = true;
            try
            {
               // Если указан только один файл
                if (cfgline.cmd.Trim().IndexOf(' ')==-1)
                {
                     string programm = string.Empty;
                     if (cfgline.cmd.Contains(".tsk")) programm = "tasker.exe";
                     if (cfgline.cmd.Contains(".ask")) programm = "asker.exe";
                     if (String.IsNullOrEmpty(programm)) 
                     {
                         log.Info(String.Format("Execute: Неопределен файл для запуска  {0}", cfgline.cmd));
                         return false;
                     }
                    //1 делаем ask
                    string fileask = File.ReadAllText(Path.Combine(appconfig.PathAsk, cfgline.cmd),Encoding.GetEncoding(1251));
                    if (!Directory.Exists(cfgline.name)) Directory.CreateDirectory(Path.Combine(appconfig.PathOut, cfgline.name));
                    string configid = Regex.Match(cfgline.id, @"(\d+)").Groups[0].Value;
                    string PathForWorkASK = Path.Combine(appconfig.PathOut, cfgline.name, PathSanitizer.SanitizeFilename(configid, '_') + "_" + cfgline.cmd);
                    File.WriteAllText(PathForWorkASK, fileask.Replace("%id", cfgline.id).Replace("%ID", cfgline.id).Replace("%file", Path.Combine(appconfig.PathOut, cfgline.name, cfgline.outfile)).
                                                                                            Replace("%FILE", Path.Combine(appconfig.PathOut, cfgline.name, cfgline.outfile)).
                                                                                            Replace("%pricecolumn", cfgline.pricefield).
                                                                                            Replace("%PRICECOLUMN", cfgline.pricefield).
                                                                                            Replace("%scheme", cfgline.scheme).
                                                                                            Replace("%SCHEME", cfgline.scheme), Encoding.GetEncoding(1251));
                    //Перед тем как выполнить аскер удалим старый файл
                    if (File.Exists(Path.Combine(appconfig.PathOut, cfgline.name, cfgline.outfile))) File.Delete(Path.Combine(appconfig.PathOut, cfgline.name, cfgline.outfile));
                    if (File.Exists(Path.Combine(appconfig.PathProgram, programm)))
                    {
                        RunProgram(Path.Combine(appconfig.PathProgram, programm), PathForWorkASK);
                    }
                    else
                    {
                        log.Error(String.Format("Файл {0} не найден в каталоге {1}",programm, appconfig.PathProgram));
                        rt = false;
                    }
                }
                else
                {
                    string Arguments = cfgline.cmd.Substring(cfgline.cmd.IndexOf(' '));
                    // Enter the executable to run, including the complete path
                    string FileName = cfgline.cmd.Split(' ')[0];
                    if (File.Exists(Path.Combine(appconfig.PathProgram, FileName)))
                    {
                        RunProgram(Path.Combine(appconfig.PathProgram, FileName), Arguments);
                    }
                    else
                    {
                        log.Error(String.Format("Файл {0} не найден в каталоге {1}", FileName, appconfig.PathProgram));
                        rt = false;
                    }
                    return rt;

                }
            }
            catch (Exception e)
            {
                log.Error(String.Format("Execute {0}", e.Message));
            }
            return true;
        }

        private void RunProgram(string Command, string argument)
        {
            try
            {
                StringCollection values = new StringCollection();
                ProcessStartInfo start = new ProcessStartInfo();
                // Enter in the command line arguments, everything you would enter after the executable name itself
                start.Arguments = argument;
                // Enter the executable to run, including the complete path
                start.FileName = Command;
                start.WorkingDirectory = Path.GetDirectoryName(start.FileName);
                // Do you want to show a console window?
                start.WindowStyle = ProcessWindowStyle.Hidden;
                start.RedirectStandardOutput = true;
                start.RedirectStandardError = true;
                start.UseShellExecute = false;
                start.CreateNoWindow = true;
                log.Trace("Запуск программы {0} {1}", start.FileName, start.Arguments);
                // Run the external process & wait for it to finish
                using (Process proc = Process.Start(start))
                {
                    proc.OutputDataReceived += (s, e) =>
                    {
                        lock (values)
                        {
                            values.Add(e.Data);
                        }
                    };
                    proc.ErrorDataReceived += (s, e) =>
                    {
                        lock (values)
                        {
                            values.Add("! > " + e.Data);
                        }
                    };

                    proc.BeginErrorReadLine();
                    proc.BeginOutputReadLine();

                    proc.WaitForExit();
                    foreach (string sline in values)
                        log.Trace("Вывод программы {0}: {1}", start.FileName, sline); ;
                }
            }
            catch(Exception e)
            {
                log.Error(String.Format("RunProgram {0}", e.Message));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Parameter">analit.cfg 8:20 1 all </param>
        /// <returns></returns>
        private List<CfgPrice> ReadConvertOption(string Parameter)
        {
            log.Trace("Читаем файлы настроек для прайсов");
            List<CfgPrice> cfgpricelines = new List<CfgPrice>();
            string maskoffiles = Parameter.Contains("cfg") ? Parameter : "*cfg";
            int dayofweek = (int) DateTime.Today.DayOfWeek;
            try
            {
                string[] files = Directory.GetFiles(appconfig.PathPricesCfg,maskoffiles);
                log.Trace("Количество найденных файлов {0}", files.Length.ToString());
                if (files.Length == 0)
                    log.Trace("Не найден ни один конфигурационный файл для конвертера в каталоге {0}", appconfig.PathPricesCfg);
                foreach (string file in files)
                {
                    try
                    {
                        log.Trace("Найден конфигурационный файл {0}", file);
                        string[] lines = File.ReadAllLines(file, Encoding.GetEncoding(1251));
                        for (int i = 0; i < lines.Length; i++)
                        {
                            #region Заполняем список
                            try
                            {
                                if (!String.IsNullOrEmpty(lines[i]) && !lines[i].Contains("#"))
                                {
                                    string[] buffer = lines[i].Split(';');
                                    string pricefield = "";
                                    if (buffer[0].Contains("("))
                                    {
                                        pricefield = Regex.Match(buffer[0], @"\(([^)]*)\)").Groups[1].Value;
                                    }
                                    //#Имя клиента (Ценовая колонка для мультиколоночного прайса);Ид из АСУ;Куда отправить;Программа с командной строкой.ask;Имя файла прайс листа;Дни запуска;Время запуска:Если нужен архив,то указать rar или zip, 
                                    
                                    CfgPrice cfgpriceitem = new CfgPrice
                                    {
                                        name = PathSanitizer.SanitizePath(buffer[0].Trim().Replace(" ", string.Empty), '_'),
                                        id = buffer[1].Trim().IndexOf(':')==-1 ? buffer[1].Trim() : buffer[1].Trim().Split(':')[0],
                                        cmd = buffer[2].Trim(),
                                        destination = buffer.Count() > 3 ? buffer[3].Trim() : "",
                                        outfile = buffer.Count() > 4 ? InsDate(buffer[4].Trim()) : "",
                                        dayofweek = buffer.Count() > 5 ? buffer[5].Trim() : "",
                                        time = buffer.Count() > 6 ? buffer[6].Trim() : "",
                                        arch = buffer.Count() > 7 ? InsDate(buffer[7].Trim()) : "",
                                        subjectLetter = buffer.Count() > 8 ? buffer[8].Trim() : "",
                                        bodyLetter = buffer.Count() > 9 ? buffer[9].Trim() : "",
                                        pricefield = pricefield,
                                        scheme = buffer[1].Trim().IndexOf(':') == -1 ? String.Empty : buffer[1].Trim().Split(':')[1]  
                                    };
                                    //Если мультиколоночный прайс, то некоторые поля не заполнены, поэтому заполняем по первой строке
                                    Regex regex = new Regex(@"^\d+$");
                                    if (!regex.IsMatch(cfgpriceitem.id))
                                    {
                                        log.Trace("ReadConvertOption строка {0}, содержит не числовое id пытаемся открыть файл {1} в каталоге ask ", lines[i], cfgpriceitem.id);
                                        cfgpriceitem.id = ReadFileWithID(Path.Combine(appconfig.PathAsk, cfgpriceitem.id));
                                    }
                                    if (!String.IsNullOrEmpty(cfgpriceitem.pricefield)
                                        && (String.IsNullOrEmpty(cfgpriceitem.dayofweek) || String.IsNullOrEmpty(cfgpriceitem.time) || String.IsNullOrEmpty(cfgpriceitem.outfile)))
                                    {
                                        CfgPrice cfgpricemulti = cfgpricelines.Find(c => c.cmd == cfgpriceitem.cmd && String.IsNullOrEmpty(c.time));
                                        cfgpriceitem.dayofweek = cfgpricemulti.dayofweek;
                                        cfgpriceitem.time = cfgpricemulti.time;
                                        cfgpriceitem.outfile = cfgpricemulti.outfile;
                                    }
                                    cfgpricelines.Add(cfgpriceitem);

                                }
                            }
                            catch (Exception e)
                            {
                                log.Error("ReadConvertOption Неверная строка {0}, конфига {1} error {2}  ", lines[i], file, e.Message);
                            }
                            #endregion 
                        }
                    }

                    catch (Exception e)
                    { log.Error("ReadConvertOption Ошибка в файле {0}, error {1}  ", file, e.Message); }
                }
                    //если указано время 8:00, то нужно отфильтровать только это время
                    if (Parameter.Contains(":"))
                    {
                        List<CfgPrice> cfgpricelinesbytime = new List<CfgPrice>();
                        foreach (var item in cfgpricelines)
                        {
                            try
                            {
                                //Поиск совпадения по времени
                                if (!String.IsNullOrEmpty(item.time))
                                {
                                    List<string> buffer = item.time.Split(' ').ToList<string>();
                                    if (buffer.FirstOrDefault(c => c == Parameter) != null)
                                        cfgpricelinesbytime.Add(item);
                                }
                            }
                            catch (Exception e)
                            {
                                log.Error("ReadConvertOption Неверная строка {0},  error {1}  ", item.name, e.Message);
                            }
                        }
                        return cfgpricelinesbytime;
                    }
                    //Если параметр день недели например 1
                    if (new Regex(@"^\d$").IsMatch(Parameter))
                    {
                        List<CfgPrice> cfgpricelinesbyday = new List<CfgPrice>();
                        int day = (int)DateTime.Today.DayOfWeek;
                        foreach (var item in cfgpricelines)
                        {
                            try
                            {
                                string[] buffer = item.dayofweek.Split('-');

                                if (day >= Convert.ToInt16(buffer[0]) && day <= Convert.ToInt16(buffer[1]))
                                    cfgpricelinesbyday.Add(item);
                            }
                            catch (Exception e)
                            {
                                log.Error("ReadConvertOption Неверная строка {0},  error {1}  ", item.name, e.Message);
                            }
                        }
                        return cfgpricelinesbyday;
                    }
                //Если параметр id 
                    if (new Regex(@"^\d+$").IsMatch(Parameter) && Parameter.Length > 1)
                    {
                        List<CfgPrice> cfgpricelinesbyid = new List<CfgPrice>();
                        foreach (var item in cfgpricelines.Where(c => c.id == Parameter))
                        {
                            try
                            {
                                if (String.IsNullOrEmpty(item.pricefield))
                                    cfgpricelinesbyid.Add(item);
                            }
                            catch (Exception e)
                            {
                                log.Error("ReadConvertOption Неверная строка {0},  error {1}  ", item.name, e.Message);
                            }
                        }

                        return cfgpricelinesbyid;
                    }
                    return cfgpricelines;
                
            }
            catch (Exception e)
            { log.Error("ReadConvertOption {0}", e.Message); }
            return cfgpricelines;
        }

        private string InsDate(string v)
        {
            var seekFormatDate = v.Split('%');
            if (seekFormatDate.Length > 1)
            {
                string Date = DateTime.Now.ToString(seekFormatDate[1]);
                return v.Replace("%",string.Empty).Replace(seekFormatDate[1], Date);
            }

            return v;
        }

        /// <summary>
        /// Процедура для чтения файла с ид , если указан вместо одного ид. Для мультиколоночного прайса
        /// </summary>
        /// <param name="namefilewithid"></param>
        /// <returns></returns>
        private string ReadFileWithID(string namefilewithid)
        {
            string rt = string.Empty;
            try
            {
           //Читаем строки яайла и соединяем  водну строку удаляя симво переводя строки игнорируя #
             List<string> buffer = File.ReadLines(namefilewithid).ToList();
             StringBuilder rtstring = new StringBuilder();
            foreach(string item in buffer)
            {
                if (item.Contains("#")) continue;
                rtstring.Append(item.Trim());
            }
            return rtstring.ToString();
            }
            catch (Exception e)
            {
                log.Error(String.Format("ReadFileWithID Ошибка чтения файла {0}  {1}", namefilewithid, e.Message));
            }
            return rt;
        }
        protected virtual bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                log.Trace("Файл {0} блокирован другим процессом", file.FullName);
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }
    }
    /// <summary>
    /// Читаем app.config
    /// </summary>
    public class ReadAppConfig
    {
        public string PathPricesCfg { get { return System.Configuration.ConfigurationManager.AppSettings["PathPricesCfg"]; } }
        public string PathOut { get { return System.Configuration.ConfigurationManager.AppSettings["PathOut"]; } }
        public string PathProgram { get { return System.Configuration.ConfigurationManager.AppSettings["PathProgram"]; } }
        public string PathAsk { get { return System.Configuration.ConfigurationManager.AppSettings["PathAsk"]; } }
        public string PathCnf { get { return System.Configuration.ConfigurationManager.AppSettings["PathCnf"]; } }
        public string rar { get { return System.Configuration.ConfigurationManager.AppSettings["rar"]; } }
        public string zip { get { return System.Configuration.ConfigurationManager.AppSettings["zip"]; } }
        public string csv { get { return System.Configuration.ConfigurationManager.AppSettings["csv"]; } }
    }
    public static class Extension
{
        /// <summary>
        /// Объединение таблиц OUTER JOIN
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="primaryKeyColumn"></param>
        /// <returns></returns>
        public static DataTable MergeAll(this IList<DataTable> tables, String primaryKeyColumn)
        {
            //if (!tables.Any())
            //    throw new ArgumentException("Tables must not be empty", "tables");
            if (primaryKeyColumn != null)
                foreach (DataTable t in tables)
                    if (!t.Columns.Contains(primaryKeyColumn))
                        throw new ArgumentException("All tables must have the specified primarykey column " + primaryKeyColumn, "primaryKeyColumn");

            if (tables.Count == 1)
                return tables[0];

            DataTable table = new DataTable("TblUnion");
            table.BeginLoadData(); // Turns off notifications, index maintenance, and constraints while loading data
            foreach (DataTable t in tables)
            {
                table.Merge(t); // same as table.Merge(t, false, MissingSchemaAction.Add);
            }
            table.EndLoadData();

            if (primaryKeyColumn != null)
            {
                // since we might have no real primary keys defined, the rows now might have repeating fields
                // so now we're going to "join" these rows ...
                var pkGroups = table.AsEnumerable()
                    .GroupBy(r => r[primaryKeyColumn]);
                var dupGroups = pkGroups.Where(g => g.Count() > 1);
                foreach (var grpDup in dupGroups)
                {
                    // use first row and modify it
                    DataRow firstRow = grpDup.First();
                    foreach (DataColumn c in table.Columns)
                    {
                        if (firstRow.IsNull(c))
                        {
                            DataRow firstNotNullRow = grpDup.Skip(1).FirstOrDefault(r => !r.IsNull(c));
                            if (firstNotNullRow != null)
                                firstRow[c] = firstNotNullRow[c];
                        }
                    }
                    // remove all but first row
                    var rowsToRemove = grpDup.Skip(1);
                    foreach (DataRow rowToRemove in rowsToRemove)
                        table.Rows.Remove(rowToRemove);
                }
            }
            return table;
        }
}

}
