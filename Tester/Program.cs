﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GetOrderForAllSources;
using System.Threading.Tasks;
using System.Configuration;
using System.Xml;
using System.Xml.XPath;    


namespace Launcher
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                ReadAppConfig appcfg = new ReadAppConfig();
                TimeSpan nowtime = DateTime.Now.TimeOfDay;
                TimeSpan starttime = new TimeSpan(Convert.ToInt16(appcfg.StartTime.Split(':')[0]), Convert.ToInt16(appcfg.StartTime.Split(':')[1]), 0);
                TimeSpan endtime = new TimeSpan(Convert.ToInt16(appcfg.EndTime.Split(':')[0]), Convert.ToInt16(appcfg.EndTime.Split(':')[1]), 0);
                if (starttime <= nowtime && nowtime <= endtime)
                {
                    Processing proc = new Processing();
                    proc.DoProcessing();
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine("{0}", e.Message);
            }
        }
    }
}
